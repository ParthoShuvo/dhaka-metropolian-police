package com.nationalappsbd.dhakametropolitanpolice.adapters.googleMapInfoWindowAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.nationalappsbd.dhakametropolitanpolice.R;

public class InfoWindowAdapterForEachSpot implements InfoWindowAdapter
        {

	private Context context;
	private View adapterView;
	private LayoutInflater layoutInflater;
	private TextView spotTitle;
	private ImageView spotIcon;
	private String[] allSpotsName;


	public InfoWindowAdapterForEachSpot(Context context) {
		this.context = context;
	}



	@SuppressLint("InflateParams")
	@Override
	public View getInfoContents(Marker marker) {
		layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		adapterView = layoutInflater.inflate(
				R.layout.google_map_info_window_adapter_layout, null);
		intializeAdapterView(adapterView);
		String spotName = marker.getTitle();
		spotTitle.setText(spotName);
        if (spotName.equals("আপনি বর্তমানে এখানে অবস্থান করছেন")) {
            spotIcon.setImageResource(R.drawable.image_user);
		} else {
            spotIcon.setImageResource(R.drawable.ic__app_launcher);
		}
		return adapterView;
	}

	@Override
	public View getInfoWindow(Marker marker) {

		return null;
	}


	public void setIcon(String spotName) {
		int position = 0;
		for (int i = 0; i < allSpotsName.length; i++) {
			if (allSpotsName[i].equalsIgnoreCase(spotName)) {
				position = i;
				break;
			}
		}
		spotIcon.setImageResource(R.drawable.ic__app_launcher);

	}


	public void intializeAdapterView(View adapterView) {
		spotTitle = (TextView) adapterView
				.findViewById(R.id.info_window_adapter_spot_title_name);
		spotIcon = (ImageView) adapterView
				.findViewById(R.id.info_window_adapter_spot_image);

	}

}
