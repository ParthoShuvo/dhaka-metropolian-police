package com.nationalappsbd.dhakametropolitanpolice.adapters.listViewAdapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ComplaintTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPPressTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPServicesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMP_PartersTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ImportantNumbersTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.NewInitiativesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.WeRememberTable;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */
public class DMPInfoListViewAdapter extends BaseAdapter {

    private String[] categoriesName;
    private String[] detailsName;
    private int LIST_VIEW_SHOW_TYPE;
    private Context context;
    private LayoutInflater layoutInflater;
    private List<NewInitiativesTable> newInitiativesTableDataList;
    private List<DMP_PartersTable> dmp_partersTableDataList;
    private int[] images;
    private List<WeRememberTable> weRememberTables;
    private List<ImportantNumbersTable> importantNumbersTableList;
    private List<DMPPressTable> dmpPressTableList;
    private List<ComplaintTable> complaintTableList;
    private List<DMPServicesTable> dmpServicesTableList;

    //LIST_VIEW_SHOW_TYPE = 1 (DMP Services)
    //LIST_VIEW_SHOW_TYPE = 2 (NEW INITIATIVES)
    //LIST_VIEW_SHOW_TYPE = 3 (DMP Partners)
    //LIST_VIEW_SHOW_TYPE = 4 (We remember)
    //LIST_VIEW_SHOW_TYPE = 5 (Emergency helpline)
    //LIST_VIEW_SHOW_TYPE = 6 (Extortion helpline)
    //LIST_VIEW_SHOW_TYPE = 7 (DMP Press)


    public DMPInfoListViewAdapter(Context context, int LIST_VIEW_SHOW_TYPE,
                                  String[] categoriesName, String[] detailsName) {

        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.LIST_VIEW_SHOW_TYPE = LIST_VIEW_SHOW_TYPE;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                this.categoriesName = categoriesName;
                this.detailsName = detailsName;
                break;

        }
    }

    public DMPInfoListViewAdapter(Context context, int LIST_VIEW_SHOW_TYPE, List dmpInfoList) {
        this.context = context;
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.LIST_VIEW_SHOW_TYPE = LIST_VIEW_SHOW_TYPE;
        TypedArray typedArray = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                dmpServicesTableList = dmpInfoList;
                break;
            case 2:
                newInitiativesTableDataList = dmpInfoList;
                break;
            case 3:
                dmp_partersTableDataList = dmpInfoList;
                typedArray = context.getResources().obtainTypedArray(R.array.dmpParters);
                images = new int[typedArray.length()];
                for (int i = 0; i < images.length; i++) {
                    images[i] = typedArray.getResourceId(i, 0);
                }
                break;
            case 4:
                weRememberTables = dmpInfoList;
                typedArray = context.getResources().obtainTypedArray(R.array.remember_image);
                images = new int[typedArray.length()];
                for (int i = 0; i < images.length; i++) {
                    images[i] = typedArray.getResourceId(i, 0);
                }
                typedArray.recycle();
                break;
            case 5:
            case 6:
                importantNumbersTableList = dmpInfoList;
                break;
            case 7:
                dmpPressTableList = dmpInfoList;
                Log.e(getClass().getName(), "Found data");
                break;
            case 8:
                complaintTableList = dmpInfoList;
                Log.e(getClass().getName(), "Found Data");
                break;


        }
    }


    @Override
    public int getCount() {
        int count = 0;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                count = dmpServicesTableList.size();
                break;
            case 2:
                count = newInitiativesTableDataList.size();
                break;
            case 3:
                count = dmp_partersTableDataList.size();
                break;
            case 4:
                count = weRememberTables.size();
                break;
            case 5:
            case 6:
                count = importantNumbersTableList.size();
                break;
            case 7:
                count = dmpPressTableList.size();
                break;
            case 8:
                count = complaintTableList.size();
                break;
        }

        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                object = dmpServicesTableList.get(position);
                break;
            case 2:
                object = newInitiativesTableDataList.get(position);
                break;
            case 3:
                object = dmp_partersTableDataList.get(position);
                break;
            case 4:
                object = weRememberTables.get(position);
                break;
            case 5:
            case 6:
                object = importantNumbersTableList.get(position);
                break;
            case 7:
                object = dmpPressTableList.get(position);
                Log.e(getClass().getName(), "Found");
                break;
            case 8:
                object = complaintTableList.get(position);
                break;


        }

        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View adapterView = null;
        if (convertView == null && layoutInflater != null) {
            switch (LIST_VIEW_SHOW_TYPE) {
                case 1:
                    adapterView = layoutInflater.inflate
                            (R.layout.category_details_layout, null, false);
                    break;
                case 2:
                    adapterView = layoutInflater.inflate
                            (R.layout.dmp_necessary_info_layout, null, false);
                    break;
                case 3:
                    adapterView = layoutInflater.inflate(R.layout.dmp_image_info_with_text_layout,
                            null, false);
                    break;
                case 4:
                case 5:
                case 6:
                    adapterView = layoutInflater.inflate(R.layout.pic_image_horizontal_layout,
                            null, false);
                    break;
                case 7:
                    adapterView = layoutInflater.inflate
                            (R.layout.category_details_layout, null, false);
                    Log.e(getClass().getName(), "view created");
                    break;
                case 8:
                    adapterView = layoutInflater.inflate(R.layout.info_view_layout,
                            null, false);
                    break;


            }
        } else {
            adapterView = convertView;
        }
        setRequiredFields(adapterView, position);
        return adapterView;
    }

    private void setRequiredFields(View adapterView, int position) {
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                setRequierdFieldsForDMPServices(adapterView, position);
                break;
            case 2:
                setRequiredFieldsForNewInitiatives(adapterView, position);
                break;
            case 3:
                setRequierdFieldsForDMPPartners(adapterView, position);
                break;
            case 4:
                setRequierdFieldsForWeRemember(adapterView, position);
                break;
            case 5:
            case 6:
                setRequiredFieldsForImportantNumbers(adapterView, position);
                break;
            case 7:
                setRequiredFieldsForDMPPress(adapterView, position);
                break;
            case 8:
                setRequierdFieldsForComplaintNumbers(adapterView, position);
                break;

        }
    }

    private void setRequierdFieldsForComplaintNumbers(View adapterView, int position) {
        TextView textView = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        textView.setText(complaintTableList.get(position).getPhNumber());
    }

    private void setRequiredFieldsForDMPPress(View adapterView, int position) {
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        txtCategoryName.setText(dmpPressTableList.get(position).getHeadline());
        txtDetails.setText("তারিখ: " + dmpPressTableList.get(position).getDate());
    }


    private void setImageView(ImageView imageView, String imageLink) {
        Picasso.with(context).load(imageLink).into(imageView);
    }

    private void setRequiredFieldsForImportantNumbers(View adapterView, int position) {
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        if (LIST_VIEW_SHOW_TYPE == 5) {
            imageView.setImageResource(R.drawable.ic_action_ic_action_error);
        } else {
            imageView.setImageResource(R.drawable.ic_action_ic_action_help);
        }
        txtDMPInfo.setText(importantNumbersTableList.get(position).getNumber());
    }

    private void setRequierdFieldsForWeRemember(View adapterView, int position) {
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        imageView.setImageResource(images[position]);
        txtDMPInfo.setText(weRememberTables.get(position).getName());
    }

    private void setRequierdFieldsForDMPPartners(View adapterView, int position) {
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        Picasso.with(context)
                .load(dmp_partersTableDataList.get(position)
                        .getImageLink())
                .error(images[position])
                .into(imageView);
        //imageView.setImageResource(images[position]);
        txtDMPInfo.setText(dmp_partersTableDataList.get(position).getParterName());

    }

    private void setRequiredFieldsForNewInitiatives(View adapterView, int position) {
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.txt_dmp_info);
        txtDMPInfo.setText(newInitiativesTableDataList.get(position).getInitiatives());
    }

    private void setRequierdFieldsForDMPServices(View adapterView, int position) {
        DMPServicesTable dmpServicesTable = dmpServicesTableList.get(position);
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        txtCategoryName.setText(dmpServicesTable.getServiceName());
        txtDetails.setText(dmpServicesTable.getDetails());
    }
}
