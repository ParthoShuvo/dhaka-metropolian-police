package com.nationalappsbd.dhakametropolitanpolice.adapters.gridViewAdapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.CommissionerListTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.MostWantedCriminalsTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.UndefinedDeadBodiesTable;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shuvojit on 5/10/15.
 */
public class DmpInfoGridViewAdapter extends BaseAdapter {

    private List<MostWantedCriminalsTable> mostWantedCriminalsTableList;
    private Context context;
    private int GRID_VIEW_SHOW_TYPE;
    private LayoutInflater layoutInflater;
    private List<UndefinedDeadBodiesTable> undefinedDeadBodiesTableList;
    private List<CommissionerListTable> commissionerListTableList;
    private int[] imageResources;

    public DmpInfoGridViewAdapter(Context context, int GRID_VIEW_SHOW_TYPE, List dmpInfoList) {
        this.context = context;
        this.GRID_VIEW_SHOW_TYPE = GRID_VIEW_SHOW_TYPE;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (GRID_VIEW_SHOW_TYPE) {
            case 1:
                this.mostWantedCriminalsTableList = dmpInfoList;

                break;
            case 2:
                this.undefinedDeadBodiesTableList = dmpInfoList;
                break;
            case 3:
                this.commissionerListTableList = dmpInfoList;
                break;
        }
        setImageResources(GRID_VIEW_SHOW_TYPE);
    }

    private void setImageResources(int grid_view_show_type) {
        TypedArray typedArray = null;
        switch (grid_view_show_type) {
            case 1:
                typedArray = context.getResources()
                        .obtainTypedArray(R.array.mostWantedCriminalsImages);
                break;
            case 3:
                typedArray = context.getResources()
                        .obtainTypedArray(R.array.commissioner_list_images);
                break;
        }
        if (typedArray != null && typedArray.length() > 0) {
            imageResources = new int[typedArray.length()];
            for (int i = 0; i < typedArray.length(); i++) {
                imageResources[i] = typedArray.getResourceId(i, 0);
            }
            typedArray.recycle();
        }
    }

    @Override
    public int getCount() {
        int size = 0;
        switch (GRID_VIEW_SHOW_TYPE) {
            case 1:
                size = mostWantedCriminalsTableList.size();
                break;
            case 2:
                size = undefinedDeadBodiesTableList.size();
                break;
            case 3:
                size = commissionerListTableList.size();
                break;
        }
        return size;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        switch (GRID_VIEW_SHOW_TYPE) {
            case 1:
                object = mostWantedCriminalsTableList.get(position);
                break;
            case 2:
                object = undefinedDeadBodiesTableList.get(position);
                break;
            case 3:
                object = commissionerListTableList.get(position);
                break;
        }
        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View adapterView = null;
        if (convertView == null) {
            switch (GRID_VIEW_SHOW_TYPE) {
                case 2:
                    adapterView = layoutInflater
                            .inflate(R.layout.info_view_layout, null);
                    break;
                case 1:
                case 3:
                    adapterView = layoutInflater
                            .inflate(R.layout.dmp_image_info_with_text_layout, null);
                    break;

            }
        } else {
            adapterView = convertView;

        }
        setFragmentElements(adapterView, position);
        return adapterView;
    }

    private void setFragmentElements(View adapterView, int position) {
        if (adapterView != null) {
            ImageView imageView = null;
            if (GRID_VIEW_SHOW_TYPE == 1 || GRID_VIEW_SHOW_TYPE == 3) {
                imageView = (ImageView) adapterView.findViewById(R.id.imageView);
            }
            TextView textView = (TextView) adapterView.findViewById(R.id.dmp_text_info);
            switch (GRID_VIEW_SHOW_TYPE) {
                case 1:
                    MostWantedCriminalsTable mostWantedCriminalsTable =
                            mostWantedCriminalsTableList.get(position);
                    Picasso.with(context)
                            .load(mostWantedCriminalsTable.getImageLink())
                            .placeholder(imageResources[position])
                            .error(imageResources[position])
                            .into(imageView);
                    textView.setText(mostWantedCriminalsTable.getName());
                    break;
                case 2:
                    UndefinedDeadBodiesTable undefinedDeadBodiesTable =
                            undefinedDeadBodiesTableList.get(position);
                   /* Picasso.with(context)
                            .load(undefinedDeadBodiesTable.getImageLink())
                            .placeholder(imageResources[position])
                            .error(imageResources[position])
                            .into(imageView);*/
                    textView.setText(undefinedDeadBodiesTable.getName());
                    break;
                case 3:
                    CommissionerListTable commissionerListTable =
                            commissionerListTableList.get(position);
                    Picasso.with(context)
                            .load(commissionerListTable.getImageLink())
                            .placeholder(imageResources[position])
                            .error(imageResources[position])
                            .into(imageView);
                    String text = commissionerListTable.getName() + "\n" +
                            commissionerListTable.getWorkingPeriod();
                    textView.setText(text);
                    break;
            }
        }
    }
}
