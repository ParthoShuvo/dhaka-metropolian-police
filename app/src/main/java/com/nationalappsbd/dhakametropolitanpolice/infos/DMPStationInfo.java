package com.nationalappsbd.dhakametropolitanpolice.infos;

/**
 * Created by shuvojit on 5/13/15.
 */
public class DMPStationInfo {

    private String stattionName;
    private String address;
    private String dutyOfficers;
    private String TandT_1;
    private String mobile_1;
    private String DMP_1;
    private String investigationCell;
    private String DMP_2;
    private String mobile_2;
    private String fax;
    private String email;
    private double latitude;
    private double longtitude;


    public DMPStationInfo(String stattionName, String address,
                          String dutyOfficers, String tandT_1,
                          String mobile_1, String DMP_1, String investigationCell,
                          String DMP_2, String mobile_2,
                          String fax, String email, double latitude,
                          double longtitude) {
        this.stattionName = stattionName;
        this.address = address;
        this.dutyOfficers = dutyOfficers;
        this.TandT_1 = tandT_1;
        this.mobile_1 = mobile_1;
        this.DMP_1 = DMP_1;
        this.investigationCell = investigationCell;
        this.DMP_2 = DMP_2;
        this.mobile_2 = mobile_2;
        this.fax = fax;
        this.email = email;
        this.latitude = latitude;
        this.longtitude = longtitude;
    }

    public DMPStationInfo() {
    }

    public String getStattionName() {
        return stattionName;
    }

    public void setStattionName(String stattionName) {
        this.stattionName = stattionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDutyOfficers() {
        return dutyOfficers;
    }

    public void setDutyOfficers(String dutyOfficers) {
        this.dutyOfficers = dutyOfficers;
    }

    public String getTandT_1() {
        return TandT_1;
    }

    public void setTandT_1(String tandT_1) {
        TandT_1 = tandT_1;
    }

    public String getMobile_1() {
        return mobile_1;
    }

    public void setMobile_1(String mobile_1) {
        this.mobile_1 = mobile_1;
    }

    public String getDMP_1() {
        return DMP_1;
    }

    public void setDMP_1(String DMP_1) {
        this.DMP_1 = DMP_1;
    }

    public String getInvestigationCell() {
        return investigationCell;
    }

    public void setInvestigationCell(String investigationCell) {
        this.investigationCell = investigationCell;
    }

    public String getDMP_2() {
        return DMP_2;
    }

    public void setDMP_2(String DMP_2) {
        this.DMP_2 = DMP_2;
    }


    public String getMobile_2() {
        return mobile_2;
    }

    public void setMobile_2(String mobile_2) {
        this.mobile_2 = mobile_2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }
}
