package com.nationalappsbd.dhakametropolitanpolice.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.fragments.DmpPoliceStationInGoogleMapFragment;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.ArrayList;

public class FragmentActivity extends ActionBarActivity implements Initializer {


    private FragmentManager fragmentManager;
    private ArrayList<PoliceThanaTable> policeThanaTableArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout_container_activity);
        initialize();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_google_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            Log.e(getClass().getName(), "Back to previous activity");

        }
        return true;
    }

    @Override
    public void initialize() {
        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolBarTextView = (TextView) actionBarToolbar.findViewById(R.id.toolBarTextView);
        actionBarToolbar.setTitle("");
        toolBarTextView.setText("মানচিত্রে থানা ");
        setSupportActionBar(actionBarToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        policeThanaTableArrayList = (ArrayList<PoliceThanaTable>) intent.
                getSerializableExtra("Police Thana Info");
        if (policeThanaTableArrayList != null && policeThanaTableArrayList.size() > 0) {
            Fragment fragment = DmpPoliceStationInGoogleMapFragment.newInstance
                    (policeThanaTableArrayList,
                            policeThanaTableArrayList.get(0).getLatitude(),
                            policeThanaTableArrayList.get(0).getLongitude(),
                            13.0f,
                            "Map Loader Activity");
            fragmentManager.beginTransaction().
                    replace(R.id.fragment_container, fragment).commit();
        }
    }
}
