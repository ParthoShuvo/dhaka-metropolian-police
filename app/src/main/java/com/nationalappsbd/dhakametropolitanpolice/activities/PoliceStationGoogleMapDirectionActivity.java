package com.nationalappsbd.dhakametropolitanpolice.activities;


import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.adapters.googleMapInfoWindowAdapters.InfoWindowAdapterForEachSpot;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.DurationDistance;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.GoogleMapDirectionJson;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.Legs;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.Location;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.Polyline;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.Routes;
import com.nationalappsbd.dhakametropolitanpolice.gsons.googleMapDirectionAPIGson.Steps;
import com.nationalappsbd.dhakametropolitanpolice.infos.ConnectivityInfo;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.DirectionApiJsonClient;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.GoogleMapClient;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;
import com.nationalappsbd.dhakametropolitanpolice.userlocations.UserLocation;
import com.nationalappsbd.dhakametropolitanpolice.webservices.WebClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PoliceStationGoogleMapDirectionActivity extends ActionBarActivity
        implements Initializer,
        GoogleMapClient, DirectionApiJsonClient {

    private UserLocation userLocation;
    private GoogleMap googleMap;
    private PoliceThanaTable policeThanaTable;
    private String distanceText;
    private String durationText;
    private String startAddress;
    private String endAddress;
    private ArrayList<LatLng> directionPointslatLng;
    private String copyRight;
    private Marker userMarker;
    private Timer timer;
    private Handler handler;
    private TimerTask timerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_google_map_layout);
        initialize();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_police_station_google_map_direction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                stopTimer();
                finish();
                Log.e(getClass().getName(), "Back to previous activity");
                break;
            case R.id.show_direction_detail_msg_menu:
                showToastMessage();
                break;
            case R.id.reload_direction:
                reloadDirection();
                break;
        }

        return true;
    }

    private void showToastMessage() {
        if (durationText != null && distanceText != null && startAddress != null &&
                endAddress != null && copyRight != null) {
            String distanceIndicator = getResources().getString
                    (R.string.direction_distance_label_text);
            String durationIndicator = getResources().getString
                    (R.string.direction_duration_label_text);
            String startAddressIndicator = getResources().getString
                    (R.string.direction_start_address_label_text);
            String endAddressIndicator = getResources().getString
                    (R.string.direction_end_address_label);
            String message = distanceIndicator + " " + distanceText + "\n" + durationIndicator +
                    " " + durationText + "\n" + startAddressIndicator + " " + startAddress + "\n"
                    + endAddressIndicator + " " + endAddress + "\n" + copyRight;
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


        } else {
            Toast.makeText(getApplicationContext(),
                    "দুঃখিত গ্রাহক কোনো পথ নির্দেশনা এ মুহূর্তে পাওয়া যাছে না",
                    Toast.LENGTH_LONG).show();

        }
        Log.e(getClass().getName(), "Message is showing");
    }


    @Override
    public void setGoogleMapZoom(double latitude, double longtitude, float cameraZoom) {
        if (googleMap != null) {
            LatLng latLng = new LatLng(latitude, longtitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, cameraZoom));
            Log.e(getClass().getName(), "Google Map has been camera zoomed");
        }
    }

    @Override
    public void setMarkerColor(Marker marker) {
        if (marker != null && policeThanaTable != null) {
            if (marker.getTitle().equals(policeThanaTable.getThanaName())) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic__app_launcher));
            } else {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_ORANGE));
            }
            Log.e(getClass().getName(), "Marker color has been added");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        userLocation = UserLocation.getUserLocation
                (PoliceStationGoogleMapDirectionActivity.this);
        userLocation.setUpUserLocation();
        double userLatitudeVal = userLocation.getUserLatitude();
        double userLongitudeVal = userLocation.getUserLongtitude();
        if (userLocation.isLocationServiceIsOn()) {
            if (userMarker == null) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLng(userLatitudeVal, userLongitudeVal))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user))
                        .title(getResources().
                                getString(R.string.user_position_indicator_title));
                userMarker = googleMap.addMarker(markerOptions);
            } else {
                changeUserPosition();
            }
            googleMap.setInfoWindowAdapter
                    (new InfoWindowAdapterForEachSpot
                            (PoliceStationGoogleMapDirectionActivity.this));
            if (!ConnectivityInfo
                    .isInternetConnectionOn(PoliceStationGoogleMapDirectionActivity.this)) {
                showUserNotifiedDialogForInternet();
            } else {
                new GoogleMapDirectionApiAsyncTask().execute();
            }
        } else {
            showUserNotifiedDialog();
        }
        setTimer();

    }

    private void showUserNotifiedDialogForInternet() {
        UserNotifiedDialog userNotifiedDialog = new UserNotifiedDialog
                (PoliceStationGoogleMapDirectionActivity.this, "Internet Connection Alert",
                        "আপনি কি আপনার ইন্টারনেট সংযোগ চালু করতে চান ");
        userNotifiedDialog.showDialog();
    }

    private void showUserNotifiedDialog() {
        UserNotifiedDialog userNotifiedDialog = new UserNotifiedDialog
                (PoliceStationGoogleMapDirectionActivity.this, "Location Service Alert",
                        "আপনি কি আপনার লোকেশন সার্ভিস চালু করতে চান ");
        userNotifiedDialog.showDialog();
    }

    private void changeUserPosition() {
        double userLatitudeVal = userLocation.getUserLatitude();
        double userLongitudeVal = userLocation.getUserLongtitude();
        LatLng markerLatLng = userMarker.getPosition();
        if (userLatitudeVal != markerLatLng.latitude ||
                userLongitudeVal != markerLatLng.longitude) {
            userMarker.setPosition(new LatLng(userLatitudeVal, userLongitudeVal));
        }
    }

    private void setTimer() {
        if (timer != null && handler != null) {
            timerTask = new TimerTask() {

                @Override
                public void run() {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {

                            reloadDirection();
                            Log.e(getClass().getName(), " timer task is running");
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(timerTask, 300000, 300000);

        }

    }

    private void reloadDirection() {
        if (userLocation.isLocationServiceIsOn()) {
            changeUserPosition();
            if (!ConnectivityInfo
                    .isInternetConnectionOn(PoliceStationGoogleMapDirectionActivity.this)) {
                showUserNotifiedDialogForInternet();
            } else {
                new GoogleMapDirectionApiAsyncTask().execute();
            }
        } else {
            showUserNotifiedDialog();
        }
    }

    private void stopTimer() {
        if (timer != null && handler != null && timerTask != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
            timerTask = null;
            handler = null;
        }
    }


    @Override
    public void addMarkerOnGoogleMap() {
        if (policeThanaTable != null) {
            LatLng latLng = new LatLng(policeThanaTable.getLatitude(),
                    policeThanaTable.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions().title
                    (policeThanaTable.getThanaName()).position(latLng).visible(true).
                    draggable(false);
            Marker marker = googleMap.addMarker(markerOptions);
            Log.e(getClass().getName(), "Marker has been added to google map");
            setMarkerColor(marker);
        } else {
            Log.e(getClass().getName(), "Null found");
        }
    }

    @Override
    public void setGoogleMapUiSettings() {
        if (googleMap != null) {
            UiSettings googleMapUiSettings = googleMap.getUiSettings();
            if (googleMapUiSettings != null) {
                googleMapUiSettings.setMapToolbarEnabled(true);
                googleMapUiSettings.setScrollGesturesEnabled(true);
                googleMapUiSettings.setZoomControlsEnabled(true);
                googleMapUiSettings.setZoomGesturesEnabled(true);
                googleMapUiSettings.setCompassEnabled(true);
                Log.e(getClass().getName(), "GoogleMap ui has been set");
            }
        }
    }

    @Override
    public void initialize() {
        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolBarTextView = (TextView) actionBarToolbar.findViewById(R.id.toolBarTextView);
        actionBarToolbar.setTitle("");
        toolBarTextView.setText("পথ নির্দেশনা");
        setSupportActionBar(actionBarToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager.
                findFragmentById(R.id.googleMap);
        googleMap = mapFragment.getMap();
        setGoogleMapUiSettings();
        Intent intent = getIntent();
        policeThanaTable = (PoliceThanaTable) intent.getSerializableExtra("Police Thana Info");
        setGoogleMapZoom(policeThanaTable.getLatitude(), policeThanaTable.getLongitude(), 11.0f);
        addMarkerOnGoogleMap();
        Log.e(getClass().getName(), "All has been initialized");
    }

    private class GoogleMapDirectionApiAsyncTask extends AsyncTask<Void, Void, Boolean> {


        private String jsonData;
        private String userLatitude;
        private String userLongitude;
        private String centerLatitude;
        private String centerLongitude;
        private UserNotifiedDialog userNotifiedDialog;


        public GoogleMapDirectionApiAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setProgressDialog();
        }

        private void setProgressDialog() {

            String msg = getResources().getString(R.string.progressbar_loading_msg);
            userNotifiedDialog = new UserNotifiedDialog
                    (PoliceStationGoogleMapDirectionActivity.this, "Loading Alert", msg);
            userNotifiedDialog.showDialog();
            Log.e(getClass().getName(), "Progress Dialog is Showing");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (policeThanaTable != null && userLocation != null) {
                centerLatitude = String.valueOf(policeThanaTable.getLatitude());
                centerLongitude = String.valueOf(policeThanaTable.getLongitude());
                userLatitude = String.valueOf(userLocation.getUserLatitude());
                userLongitude = String.valueOf(userLocation.getUserLongtitude());
                String googleMapDirectionWebUrl = DIRECTION_API_WEB_ADDRESS_PART_1 + userLatitude +
                        "," + userLongitude + DIRECTION_API_WEB_ADDRESS_PART_2 + centerLatitude +
                        "," + centerLongitude + DIRECTION_API_WEB_ADDRESS_PART_3;
                WebClient webClient = new WebClient(googleMapDirectionWebUrl);
                Log.e(getClass().getName(), "Sending url to web client for getting json data");
                jsonData = webClient.getJSONStringData();
                if (jsonData != null) {
                    return true;
                }

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (jsonData != null && result == true) {
                Log.e(getClass().getName(), "Data fetching Successfully");
                Gson gson = new Gson();
                GoogleMapDirectionJson googleMapDirectionJson =
                        gson.fromJson(jsonData, GoogleMapDirectionJson.class);
                getAllDirectionRequiredVal(googleMapDirectionJson);

            } else {
                Log.e(getClass().getName(), "Data fetching failed");
            }
            if (userNotifiedDialog != null && userNotifiedDialog.isShowing()) {
                userNotifiedDialog.closeDialog();
                showToastMessage();
            }

        }

        private void getAllDirectionRequiredVal(GoogleMapDirectionJson googleMapDirectionJson) {
            ArrayList<Routes> routesArrayList = googleMapDirectionJson.getRoutesArrayList();
            if (routesArrayList != null && routesArrayList.size() > 0) {
                Routes routes = routesArrayList.get(0);
                ArrayList<Legs> legsArrayList = routes.getLegsArrayList();
                if (legsArrayList != null && legsArrayList.size() > 0) {
                    Legs legs = legsArrayList.get(0);
                    startAddress = getStartAddress(legs);
                    endAddress = getEndAddress(legs);
                    durationText = getDurationText(legs);
                    distanceText = getDistanceText(legs);

                    directionPointslatLng = getPolyLinePoints(legs);
                }
                copyRight = getCopyRight(routes);

            }

            if (distanceText != null && durationText != null && startAddress != null &&
                    endAddress != null && directionPointslatLng != null &&
                    directionPointslatLng.size() > 0 && copyRight != null) {
                Log.e(getClass().getName(), "All steps has been found.");
                PolylineOptions polylineOptions = new PolylineOptions().width(5).visible(true)
                        .color(Color.MAGENTA);
                for (int i = 0; i < directionPointslatLng.size(); i++) {
                    polylineOptions.add(directionPointslatLng.get(i));
                }
                googleMap.addPolyline(polylineOptions);
            }


        }

        private String getCopyRight(Routes routes) {
            String copyRight = null;
            copyRight = routes.getCopyrights();
            return copyRight;
        }

        private String getEndAddress(Legs legs) {
            String endAddress = null;
            endAddress = legs.getEndAddress();
            return endAddress;
        }

        private String getStartAddress(Legs legs) {
            String startAddress = null;
            startAddress = legs.getStartAddress();
            return startAddress;

        }


        private String getDurationText(Legs legs) {
            String durationText = null;
            DurationDistance durationDistance = legs.getDuration();
            durationText = durationDistance.getText();
            return durationText;
        }

        private String getDistanceText(Legs legs) {
            String distanceText = null;
            DurationDistance distance = legs.getDistance();
            distanceText = distance.getText();
            return distanceText;
        }

        private ArrayList<LatLng> getPolyLinePoints(Legs legs) {
            ArrayList<LatLng> polyLines = null;
            Location startLocation = null;
            Location endLocation = null;
            Polyline polyLine = null;
            Steps steps = null;
            ArrayList<LatLng> decodePolyLineLatLngs;
            double lat = 0;
            double lng = 0;

            ArrayList<Steps> stepsArrayList = legs.getStepsArrayList();
            if (stepsArrayList != null) {
                Log.e(getClass().getName(), "All steps has been found.");
                polyLines = new ArrayList<LatLng>();
                for (int i = 0; i < stepsArrayList.size(); i++) {
                    steps = stepsArrayList.get(i);
                    startLocation = steps.getStartLocation();
                    lat = startLocation.getLat();
                    lng = startLocation.getLng();
                    polyLines.add(new LatLng(lat, lng));
                    polyLine = steps.getPolyline();
                    decodePolyLineLatLngs = decodePoly(polyLine.getPoints());
                    if (decodePolyLineLatLngs != null && decodePolyLineLatLngs.size() > 0) {
                        for (int j = 0; j < decodePolyLineLatLngs.size(); j++) {
                            LatLng latLng = decodePolyLineLatLngs.get(j);
                            polyLines.add(latLng);
                        }
                    }
                    endLocation = steps.getEndLocation();
                    lat = endLocation.getLat();
                    lng = endLocation.getLng();
                    polyLines.add(new LatLng(lat, lng));
                }
            }

            return polyLines;
        }

        private ArrayList<LatLng> decodePoly(String encoded) {
            ArrayList<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;
            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;
                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng position = new LatLng((double) lat / 1E5,
                        (double) lng / 1E5);
                poly.add(position);
            }
            return poly;
        }

    }

}
