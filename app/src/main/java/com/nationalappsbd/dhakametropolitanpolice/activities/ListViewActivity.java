package com.nationalappsbd.dhakametropolitanpolice.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.adapters.listViewAdapters.DMPInfoListViewAdapter;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ImportantNumbersTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.List;

public class ListViewActivity extends ActionBarActivity implements Initializer, AdapterView.OnItemClickListener {

    private int ACTIVITY_SHOW_TYPE;
    private List<ImportantNumbersTable> importantNumbersTableList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_list_view);
            initialize();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolBarTextView = (TextView) toolbar.findViewById(R.id.toolBarTextView);
        toolbar.setTitle("");
        toolBarTextView.setText(getIntent().getStringExtra("ACTION_BAR_NAME"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView listView = (ListView) findViewById(R.id.dmp_info_list_view);
        Intent intent = getIntent();
        ACTIVITY_SHOW_TYPE = intent.getIntExtra("ACTIVITY_SHOW_TYPE", 0);
        DMPInfoListViewAdapter dmpInfoListViewAdapter = null;
        switch (ACTIVITY_SHOW_TYPE) {
            case 1:
                importantNumbersTableList = ImportantNumbersTable.
                        importantNumbersTableDataList("emergency");
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(ListViewActivity.this, 5,
                        importantNumbersTableList);
                break;
            case 2:
                importantNumbersTableList = ImportantNumbersTable.
                        importantNumbersTableDataList("extortion");
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(ListViewActivity.this, 6,
                        importantNumbersTableList);
                break;
        }
        if (dmpInfoListViewAdapter != null) {
            listView.setAdapter(dmpInfoListViewAdapter);
        }
        listView.setOnItemClickListener(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserNotifiedDialog userNotifiedDialog = null;
        String number = null;
        number = importantNumbersTableList.get(position).getNumber();
        if (number.charAt(0) == '9') {
            number = "02" + number;
        }
        switch (ACTIVITY_SHOW_TYPE) {
            case 1:

                userNotifiedDialog = new UserNotifiedDialog
                        (ListViewActivity.this, "Calling Alert",
                                "জরুরি সাহায্যের জন্য আপনার কল চার্জ প্রযোজ্য হবে",
                                number);
                userNotifiedDialog.showDialog();
                break;
            case 2:

                userNotifiedDialog = new UserNotifiedDialog
                        (ListViewActivity.this, "Calling Alert",
                                "হেল্প লাইন নাম্বারে কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ",
                                number);
                userNotifiedDialog.showDialog();
                break;
        }
    }
}
