package com.nationalappsbd.dhakametropolitanpolice.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.adapters.listViewAdapters.NavDrawerListViewAdapter;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceClearanceCertificateTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.fragments.DMPNecessaryInfoFragment;
import com.nationalappsbd.dhakametropolitanpolice.fragments.DmpPoliceStationInGoogleMapFragment;
import com.nationalappsbd.dhakametropolitanpolice.fragments.PoliceStationSelectionFragment;
import com.nationalappsbd.dhakametropolitanpolice.fragments.SlidingTabViewFragment;
import com.nationalappsbd.dhakametropolitanpolice.fragments.WebViewFragment;
import com.nationalappsbd.dhakametropolitanpolice.infos.ConnectivityInfo;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements Initializer,
        AdapterView.OnItemClickListener {

    private final String SEARCH_COLUMN_NAME = "policeStationName";
    private FragmentManager fragmentManager;
    private DrawerLayout navDrawerLayout;
    private ListView navDrawerListView;
    private ActionBarDrawerToggle navDrawerListener;
    private NavDrawerListViewAdapter navDrawerListViewItemAdapter;
    private String[] navDrawerMenuItemNames;
    private Fragment fragment;
    private int menuPosition = -1;
    private SimpleCursorAdapter cursorAdapter;
    private MatrixCursor matrixCursor;
    private Toolbar toolbar;
    private SearchView searchView;
    private TextView toolBarTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setContentView(R.layout.activity_main);
            initialize();
            setNavDrawerListViewAdapter();
            navDrawerListView.setItemChecked(0, true);
            setCursorAdapter();
        }
    }


    private void setCursorAdapter() {
        final String[] from = new String[]{SEARCH_COLUMN_NAME};
        final int[] to = new int[]{android.R.id.text1};
        cursorAdapter = new SimpleCursorAdapter(MainActivity.this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView queryTextView = (TextView) view.findViewById(android.R.id.text1);
                queryTextView.setTextColor(getResources().getColor(R.color.colorAccent));
                queryTextView.setBackgroundColor(getResources().getColor(R.color.white));
                return view;
            }
        };
    }


    private void setNavDrawerListViewAdapter() {
        if (navDrawerListViewItemAdapter != null) {
            navDrawerListView.setAdapter(navDrawerListViewItemAdapter);
            navDrawerListView.setOnItemClickListener(MainActivity.this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setSuggestionsAdapter(cursorAdapter);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int i) {
                String s = matrixCursor.getString(matrixCursor
                        .getColumnIndex(SEARCH_COLUMN_NAME));
                searchView.setQuery(s, false);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String searchQuery) {

                List<PoliceThanaTable> cycloneCenterInfoList = PoliceThanaTable
                        .getAllPoliceThanaList(searchQuery);
                if (cycloneCenterInfoList != null && cycloneCenterInfoList.size() > 0) {
                    showCycloneCenterList(cycloneCenterInfoList);
                    cursorAdapter.changeCursor(null);
                } else {
                    String toastMessage = searchQuery + " এই নামে কোনো পুলিশ থানা খুঁজে পাওয়া যায় নি";
                    Toast.makeText(MainActivity.this, toastMessage,
                            Toast.LENGTH_LONG).show();
                }
                return true;
            }


            @Override
            public boolean onQueryTextChange(String s) {
                populateAdapter(s);
                return false;
            }
        });

        return true;
    }

    private void showCycloneCenterList(List<PoliceThanaTable> policeThanaTableList) {
        Intent intent = new Intent(MainActivity.this, PoliceStationInfoActivity.class);
        intent.putExtra("Police Thana Info",
                (ArrayList<PoliceThanaTable>) policeThanaTableList);
        intent.putExtra("ACTION_BAR_NAME", "পুলিশ থানাসমুহ");
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            searchView.setQuery("", false);
            searchView.setIconified(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateAdapter(String query) {
        if (query.length() >= 2) {
            List<String> policeThanaNameList = PoliceThanaTable
                    .getAllPoliceThanaNameList(query);
            if (policeThanaNameList != null && policeThanaNameList.size() > 0) {
                matrixCursor = new
                        MatrixCursor(new String[]{BaseColumns._ID, SEARCH_COLUMN_NAME});
                for (int i = 0; i < policeThanaNameList.size(); i++) {
                    matrixCursor.addRow(new Object[]{i, policeThanaNameList.get(i)});
                }
                cursorAdapter.changeCursor(matrixCursor);
            } else {
                cursorAdapter.changeCursor(null);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (navDrawerListener != null) {
            navDrawerListener.onOptionsItemSelected(item);
        }
        int id = item.getItemId();
        Intent intent = null;
        UserNotifiedDialog userNotifiedDialog = null;
        String msg = null;
        switch (id) {

            case R.id.search_menu:
                navDrawerLayout.closeDrawers();

                onSearchRequested();
                break;
            case R.id.emergency_help_menu:
                intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra("ACTIVITY_SHOW_TYPE", 1);
                intent.putExtra("ACTION_BAR_NAME", getResources()
                        .getString(R.string.emergency_help_menu));
                startActivity(intent);
                break;
            case R.id.extortion_help_menu:
                intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra("ACTIVITY_SHOW_TYPE", 2);
                intent.putExtra("ACTION_BAR_NAME", getResources()
                        .getString(R.string.extortion_help_menu));
                startActivity(intent);
                break;
            case R.id.web_portal_menu:
                if (!ConnectivityInfo
                        .isInternetConnectionOn(MainActivity.this)) {
                    showUserNotifiedDialogForInternet();
                } else {
                    fragment = WebViewFragment.newInstance("http://dmpnews.org/");
                    getSupportFragmentManager().beginTransaction().
                            replace(R.id.fragment_container, fragment).commit();
                    navDrawerListView.setItemChecked(menuPosition, false);
                    toolBarTextView.setText(R.string.web_portal_menu);
                    menuPosition = -1;
                }
                break;
            case R.id.facebookMenu:
                if (!ConnectivityInfo
                        .isInternetConnectionOn(MainActivity.this)) {
                    showUserNotifiedDialogForInternet();
                } else {
                    fragment = WebViewFragment.newInstance("https://www.facebook.com/dmp.dhaka?_rdr");
                    getSupportFragmentManager().beginTransaction().
                            replace(R.id.fragment_container, fragment).commit();
                    navDrawerListView.setItemChecked(menuPosition, false);
                    toolBarTextView.setText(R.string.facebookMenu);
                    menuPosition = -1;
                }
                break;

            case R.id.about_menu:
                msg = "এই অ্যাপ্লিকেশন ঢাকা মেট্রোপলিটন পুলিশের জন্য";
                userNotifiedDialog =
                        new UserNotifiedDialog(MainActivity.this, "Description Alert", msg);
                userNotifiedDialog.showDialog();
                break;
            case R.id.updateDataMenu:
                if (!ConnectivityInfo
                        .isInternetConnectionOn(MainActivity.this)) {
                    showUserNotifiedDialogForInternet();
                } else {
                    msg = "আপনি কি তথ্য আপডেট করতে চান ?";
                    userNotifiedDialog = new UserNotifiedDialog(MainActivity.this,
                            "Service Starting Alert", msg);
                    userNotifiedDialog.showDialog();
                }
                break;
            case R.id.clearance_certificate_menu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    String url = PoliceClearanceCertificateTable
                            .getPoliceClearanceCertificateTableData()
                            .getUrlLink();
                    Log.e(getClass().getName(), url);
                    if (url != null && !url.equals("")) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }

                }
                break;
            case android.R.id.home:
                Log.e(getClass().getName(), "Clicked");
                break;


        }
        return true;
    }

    private void showUserNotifiedDialogForInternet() {
        UserNotifiedDialog userNotifiedDialog = new UserNotifiedDialog
                (MainActivity.this, "Internet Connection Alert",
                        "আপনি কি আপনার ইন্টারনেট সংযোগ চালু করতে চান ");
        userNotifiedDialog.showDialog();
    }

    @Override
    public void initialize() {
        toolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        toolbar.setTitle("");
        toolBarTextView = (TextView) toolbar.findViewById(R.id.toolBarTextView);
        fragmentManager = getSupportFragmentManager();
        setSupportActionBar(toolbar);
        navDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
        navDrawerListView = (ListView) findViewById(R.id.nav_drawer_list_menu);
        setNavigationDrawerListener();
        navDrawerListViewItemAdapter = new NavDrawerListViewAdapter(MainActivity.this);
        navDrawerMenuItemNames = getResources().getStringArray(R.array.navMenuItem);
        setFragment(0);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setNavigationDrawerListener() {
        int drawerOpenMsg = R.string.nav_drawer_open_msg;
        int drawerCloseMsg = R.string.nav_drawer_close_msg;
        navDrawerListener = new ActionBarDrawerToggle(MainActivity.this,
                navDrawerLayout, drawerOpenMsg, drawerCloseMsg) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.e(getClass().getName(), "Navigation drawer is opened");

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.e(getClass().getName(), "Navigation drawer closed");

            }
        };

        if (navDrawerListener != null && navDrawerLayout != null) {
            Log.e(getClass().getName(), "Navigation drawer listener has been added");
            navDrawerListener.setHomeAsUpIndicator(R.drawable.ic_drawer);
            navDrawerLayout.setDrawerListener(navDrawerListener);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (navDrawerListener != null) {
            navDrawerListener.onConfigurationChanged(newConfig);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (navDrawerListener != null) {
            navDrawerListener.syncState();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e(getClass().getName(), navDrawerMenuItemNames[position] + "Clicked");
        setFragment(position);
        navDrawerLayout.closeDrawers();
    }

    private void setFragment(int position) {
        String menuName = navDrawerMenuItemNames[position];
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        boolean fragmentChangeFlag = false;
        switch (menuName) {
            case "মূলপাতা":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance("Main Page");
                    menuPosition = position;
                    fragmentChangeFlag = true;
                }
                break;
            case "ডিএমপি কমিশনার":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance("DMP Commissioner");
                    menuPosition = position;
                    fragmentChangeFlag = true;
                }
                break;
            case "ডিএমপি সম্পর্কে":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance("About DMP");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "ডিএমপির ইউনিট":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance("DMP Units");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "আমরা স্মরণ করি":
                if (position != menuPosition) {
                    fragment = DMPNecessaryInfoFragment.getNewInstance(1, "We Remember");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "অপরাধের তথ্য":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance("Crime Info");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "মানচিত্রে থানাসমুহ":
                if (position != menuPosition) {
                    List<PoliceThanaTable> policeThanaTableList = PoliceThanaTable.
                            getAllPoliceThanaList();
                    if (policeThanaTableList != null && policeThanaTableList.size() > 0) {
                        fragment = DmpPoliceStationInGoogleMapFragment.
                                newInstance((ArrayList<PoliceThanaTable>)
                                                policeThanaTableList,
                                        policeThanaTableList.get(41).getLatitude(),
                                        policeThanaTableList.get(41).getLongitude(), 11f);
                        Log.e(getClass().getName(), "Found");
                    }

                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "সংবাদ বিজ্ঞপ্তি":
                if (position != menuPosition) {
                    fragment = DMPNecessaryInfoFragment.getNewInstance(6, "DMP Press");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "নাগরিক সাহায্য":
                if (position != menuPosition) {

                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "অভিযোগ":
                if (position != menuPosition) {
                    fragment = DMPNecessaryInfoFragment.getNewInstance(7, "Complaint");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;

            case "পুলিশ থানাসমুহ":
                if (position != menuPosition) {
                    fragment = PoliceStationSelectionFragment.getNewInstance();
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;

        }
        if (fragmentChangeFlag) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
            toolBarTextView.setText(menuName);
        }
    }

    @Override
    public void onBackPressed() {
        if (menuPosition != 0) {
            setFragment(0);
        } else {
            String msg = "আপনি এই অ্যাপ থেকে প্রস্থান করবেন ?";
            UserNotifiedDialog userNotifiedDialog =
                    new UserNotifiedDialog(MainActivity.this, "Exiting Alert", msg);
            userNotifiedDialog.showDialog();
        }
    }
}
