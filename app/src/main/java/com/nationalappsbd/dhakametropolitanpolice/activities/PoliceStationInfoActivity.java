package com.nationalappsbd.dhakametropolitanpolice.activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPPressTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.WeRememberTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.infos.ConnectivityInfo;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.ArrayList;
import java.util.List;

public class PoliceStationInfoActivity extends ActionBarActivity implements Initializer,
        View.OnClickListener {

    private TextView txtThanaName;
    private TextView txtAddress;
    private TextView txtDMP1;
    private TextView txtCell;
    private TextView txtTAndT;
    private TextView txtInspectorInvestigationCell;
    private TextView txtDMP2;
    private TextView txtMobile;
    private TextView txtFax;
    private TextView txtEmail;
    private TextView txtPersonName;
    private TextView txtTimeLineForJob;
    private TextView txtDetails;
    private TextView txtDesignation;
    private ImageView imageView;
    private ArrayList<PoliceThanaTable> policeThanaTableDataList;
    private PoliceThanaTable policeThanaTable;
    private WeRememberTable weRememberTable;
    private int imageResources[];
    private MenuItem mapMenu;
    private MenuItem directionMenu;
    private DMPPressTable dmpPressTable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        policeThanaTableDataList = (ArrayList<PoliceThanaTable>)
                intent.getSerializableExtra("Police Thana Info");
        weRememberTable = (WeRememberTable) intent.getSerializableExtra("We Remember");
        dmpPressTable = (DMPPressTable) intent.getSerializableExtra("DMP Press");
        if (policeThanaTableDataList != null) {
            setContentView(R.layout.activity_police_station_info);
            initialize();
            txtCell.setOnClickListener(this);
            txtMobile.setOnClickListener(this);
            txtTAndT.setOnClickListener(this);
            txtInspectorInvestigationCell.setOnClickListener(this);
        } else if (weRememberTable != null) {
            setContentView(R.layout.we_remember_layout);
            TypedArray typedArray = getResources().obtainTypedArray(R.array.remember_image);
            imageResources = new int[typedArray.length()];
            for (int i = 0; i < typedArray.length(); i++) {
                imageResources[i] = typedArray.getResourceId(i, 0);

            }
            typedArray.recycle();
            initialize();
            int position = intent.getIntExtra("Position", 0);
            imageView.setImageResource(imageResources[position]);
        } else if (dmpPressTable != null) {
            setContentView(R.layout.dmp_press_info_layout);
            initialize();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_police_station_info, menu);
        mapMenu = menu.findItem(R.id.google_map_menu);
        directionMenu = menu.findItem(R.id.google_map_direction_menu);
        if (policeThanaTableDataList != null) {
            mapMenu.setVisible(true);
            directionMenu.setVisible(true);
        } else {
            mapMenu.setVisible(false);
            directionMenu.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();

        } else if (id == R.id.google_map_menu) {
            googleMapMenuInitalize();
        } else {
            googleMapDirectionMenuInitialize();
        }
        return true;
    }

    @Override
    public void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolBarTextView = (TextView) toolbar.findViewById(R.id.toolBarTextView);
        toolbar.setTitle("");
        toolBarTextView.setText(getIntent().getStringExtra("ACTION_BAR_NAME"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (policeThanaTableDataList != null) {
            txtThanaName = (TextView) findViewById(R.id.txt_police_station_name);
            txtAddress = (TextView) findViewById(R.id.txtAddress);
            txtDMP1 = (TextView) findViewById(R.id.txtDMP1);
            txtCell = (TextView) findViewById(R.id.txtCell);
            txtTAndT = (TextView) findViewById(R.id.txtTAndT);
            txtInspectorInvestigationCell = (TextView)
                    findViewById(R.id.txtInspectorInvestigationCell);
            txtDMP2 = (TextView) findViewById(R.id.txtDMP2);
            txtMobile = (TextView) findViewById(R.id.txtMobile);
            txtFax = (TextView) findViewById(R.id.txtFax);
            txtEmail = (TextView) findViewById(R.id.txtEmail);
            txtEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ConnectivityInfo.isInternetConnectionOn(PoliceStationInfoActivity.this)) {
                        Toast.makeText(PoliceStationInfoActivity.this,
                                "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                    } else {
                        Intent testIntent = new Intent(Intent.ACTION_VIEW);
                        Uri data = Uri.parse
                                ("mailto:?subject=" + "Dhaka MetroPolitan Police" +
                                        "&body=" + "" + "&to=" + txtEmail.getText().toString());
                        testIntent.setData(data);
                        startActivity(testIntent);
                    }
                }
            });
            Intent intent = getIntent();
            policeThanaTableDataList = (ArrayList<PoliceThanaTable>)
                    intent.getSerializableExtra("Police Thana Info");
            setAllElementValues(policeThanaTableDataList);
        } else if (weRememberTable != null) {
            imageView = (ImageView) findViewById(R.id.imageView);
            txtPersonName = (TextView) findViewById(R.id.dmp_text_info);
            txtTimeLineForJob = (TextView) findViewById(R.id.txtTimeLineForJob);
            txtDetails = (TextView) findViewById(R.id.txtDetails);
            txtPersonName.setText(weRememberTable.getName());
            txtDesignation = (TextView) findViewById(R.id.txtDesignation);
            txtDesignation.setText(weRememberTable.getDesignation());
            txtTimeLineForJob.setText(weRememberTable.getFromDate() + " থেকে "
                    + weRememberTable.getToDate());
            txtDetails.setText(weRememberTable.getDetails().trim());
            txtDetails.setGravity(Gravity.LEFT);


        } else if (dmpPressTable != null) {
            TextView txtCategory = (TextView) findViewById(R.id.txt_category);
            TextView txtDetails = (TextView) findViewById(R.id.txt_details);
            txtCategory.setText(dmpPressTable.getHeadline());
            String details = "তারিখ:" + " " + dmpPressTable.getDate()
                    + "\n\n" + dmpPressTable.getDetails();
            //Log.e(getClass().getName(), dmpPressTable.getHeadline() + dmpPressTable.getDetails());
            txtDetails.setText(details);

        }


    }

    private void setAllElementValues(List<PoliceThanaTable> policeThanaTableDataList) {
        if (policeThanaTableDataList != null && policeThanaTableDataList.size() > 0) {
            policeThanaTable = policeThanaTableDataList.get(0);
            txtThanaName.setText(policeThanaTable.getThanaName());
            if (!policeThanaTable.getAddress().equals("নাই")) {
                txtAddress.setText(policeThanaTable.getAddress());
            }
            if (!policeThanaTable.getDmp_1().equals("নাই")) {
                txtDMP1.setText(policeThanaTable.getDmp_1());
            }
            if (!policeThanaTable.getDmp_2().equals("নাই")) {
                txtDMP2.setText(policeThanaTable.getDmp_2());
            }
            if (!policeThanaTable.getCell().equals("নাই")) {
                txtCell.setText(policeThanaTable.getCell());
            }
            if (!policeThanaTable.getT_and_t().equals("নাই")) {
                txtTAndT.setText(policeThanaTable.getT_and_t());
            }
            if (!policeThanaTable.getInspectorInvestigationCell().equals("নাই")) {
                txtInspectorInvestigationCell
                        .setText(policeThanaTable.getInspectorInvestigationCell());
            }
            if (!policeThanaTable.getMobile().equals("নাই")) {
                txtMobile.setText(policeThanaTable.getMobile());
            }
            if (!policeThanaTable.getMail().equals("নাই")) {
                txtEmail.setText(policeThanaTable.getMail());
            }
            if (!policeThanaTable.getFax().equals("নাই")) {
                txtFax.setText(policeThanaTable.getFax());
            }

        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        String phoneNum = null;
        String msg = "তে কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
        boolean call = false;
        switch (id) {
            case R.id.txtMobile:
                if (!policeThanaTable.getMobile().equals("নাই")) {
                    txtMobile.setText(policeThanaTable.getMobile());
                    call = true;
                    phoneNum = policeThanaTable.getMobile();
                }
                break;
            case R.id.txtCell:
                if (!policeThanaTable.getCell().equals("নাই")) {
                    txtCell.setText(policeThanaTable.getCell());
                    call = true;
                    phoneNum = policeThanaTable.getCell();
                }
                break;
            case R.id.txtTAndT:
                if (!policeThanaTable.getCell().equals("নাই")) {
                    txtCell.setText(policeThanaTable.getCell());
                    call = true;
                    phoneNum = policeThanaTable.getT_and_t();
                }
                break;
            case R.id.txtInspectorInvestigationCell:
                if (!policeThanaTable.getInspectorInvestigationCell().equals("নাই")) {
                    txtInspectorInvestigationCell
                            .setText(policeThanaTable.getInspectorInvestigationCell());
                    call = true;
                    phoneNum = policeThanaTable.getInspectorInvestigationCell();
                }
                break;

        }
        if (call) {
            msg = policeThanaTable.getThanaName() + " " + msg;
            UserNotifiedDialog userNotifiedDialog = new
                    UserNotifiedDialog(PoliceStationInfoActivity.this,
                    "Calling Alert", msg, phoneNum);
            userNotifiedDialog.showDialog();
        }
    }

    private void googleMapMenuInitalize() {
        PoliceThanaTable policeThanaTable = policeThanaTableDataList.get(0);
        if (policeThanaTable.getLongitude() == 0.0 || policeThanaTable.getLatitude() == 0.0) {
            Toast.makeText(PoliceStationInfoActivity.this,
                    "এই থানার অক্ষাংশ ও দ্রাঘিমা এইই মুহতে পাওয়া যাছে ", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(PoliceStationInfoActivity.this,
                    FragmentActivity.class);
            intent.putExtra("Police Thana Info", policeThanaTableDataList);
            startActivity(intent);
            Log.e(getClass().getName(),
                    "Cyclone Center google Map has been Menu has been started");
        }
    }

    private void googleMapDirectionMenuInitialize() {
        if (policeThanaTable.getLongitude() == 0.0 || policeThanaTable.getLatitude() == 0.0) {
            Toast.makeText(PoliceStationInfoActivity.this,
                    "এই থানার অক্ষাংশ ও দ্রাঘিমা এইই মুহতে পাওয়া যাছে ", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(PoliceStationInfoActivity.this,
                    PoliceStationGoogleMapDirectionActivity.class);
            PoliceThanaTable policeThanaTable = policeThanaTableDataList.get(0);
            intent.putExtra("Police Thana Info", policeThanaTable);
            startActivity(intent);
            Log.e(getClass().getName(),
                    "Cyclone Center google Map direction has been Menu has been " +
                            "started");
        }
    }
}
