package com.nationalappsbd.dhakametropolitanpolice.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.backgroundServices.BackGroundAsynTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by shuvojit on 4/30/15.
 */
public class UserNotifiedDialog {

    public static Context context;
    private String dialogType;
    private String msg;
    private SweetAlertDialog sweetAlertDialog;
    private String phoneNumber;


    public UserNotifiedDialog(Context context, final String DIALOG_TYPE, String msg) {
        this.context = context;
        this.msg = msg;
        dialogType = DIALOG_TYPE;

    }

    public UserNotifiedDialog(
            Context context, final String DIALOG_TYPE, String msg, String phoneNumber) {
        this.context = context;
        this.msg = msg;
        dialogType = DIALOG_TYPE;
        this.phoneNumber = phoneNumber;
    }

    public void showDialog() {
        if (context != null && msg != null) {
            sweetAlertDialog = new SweetAlertDialog(context);
            if (dialogType.equals("Searching Alert")) {
                setProgressDialogForSearchingAlert(sweetAlertDialog);

            } else if (dialogType.equals("Exiting Alert")) {
                setProgressDialogForExitingAlert(sweetAlertDialog);
            } else if (dialogType.equals("Loading Alert")) {
                setProgressDialogForLoadingAlert(sweetAlertDialog);
            } else if (dialogType.equals("Calling Alert")) {
                setProgressDialogForCallingAlert(sweetAlertDialog);
            } else if (dialogType.equals("Description Alert")) {
                setProgressDialogForDescriptionAlert(sweetAlertDialog);
            } else if (dialogType.equals("Service Starting Alert")) {
                setProgressDialogForServiceStartAlert(sweetAlertDialog);
            } else if (dialogType.equals("Location Service Alert")) {
                setProgressDialogForLocationServiceStartAlert(sweetAlertDialog);
            } else if (dialogType.equals("Internet Connection Alert")) {
                setProgressDialogForInternetStartAlert();
            }
            Log.e(getClass().getName(), "Dialog is shown");
        }
    }

    private void setProgressDialogForInternetStartAlert() {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText("ইন্টারনেট সংযোগ বন্ধ");
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setCancelText("না");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setConfirmText("হাঁ");
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    closeDialog();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(final SweetAlertDialog sweetAlertDialog) {
                    openInterentSettingsActivity();
                    closeDialog();

                }
            });
            sweetAlertDialog.show();
        }

    }

    private void openInterentSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        context.startActivity(intent);
    }

    private void setProgressDialogForLocationServiceStartAlert(SweetAlertDialog sweetAlertDialog) {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText("লোকেশন সার্ভিস বন্ধ");
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setCancelText("না");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setConfirmText("হাঁ");
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    closeDialog();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(final SweetAlertDialog sweetAlertDialog) {
                    openLocationSettingsActivity();
                    closeDialog();

                }
            });
            sweetAlertDialog.show();
        }

    }

    private void openLocationSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
    }

    private void setProgressDialogForServiceStartAlert(SweetAlertDialog sweetAlertDialog) {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);
            sweetAlertDialog.setCustomImage(R.drawable.ic_action_refresh);
            sweetAlertDialog.setTitleText(context.getResources().getString(R.string.updateDataMenu));
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setCancelText("না");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setConfirmText("হাঁ");
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    closeDialog();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(final SweetAlertDialog sweetAlertDialog) {
                    final SweetAlertDialog sweetProgressAlertDialog = new
                            SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    sweetProgressAlertDialog
                            .setTitleText(context.getResources()
                                    .getString(R.string.updateStartingNotifier));
                    sweetProgressAlertDialog.getProgressHelper()
                            .setBarColor(context.getResources()
                                    .getColor(R.color.colorAccent));
                    sweetProgressAlertDialog.setCancelable(false);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sweetProgressAlertDialog.dismissWithAnimation();
                            new BackGroundAsynTask().execute();
                            Log.e(getClass().getName(), "background Services has been started");
                        }
                    }, 5000);
                    closeDialog();
                    sweetProgressAlertDialog.show();

                }
            });
            sweetAlertDialog.show();
        }
    }


    private void setProgressDialogForDescriptionAlert(SweetAlertDialog sweetAlertDialog) {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCustomImage(R.drawable.ic__app_launcher);
            sweetAlertDialog.setTitleText(context.getResources().getString(R.string.app_name));
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setConfirmText("বন্ধ করুন");
            sweetAlertDialog.showCancelButton(false);
            Log.e(getClass().getName(), "Showing");
            sweetAlertDialog.show();
        }
    }

    private void setProgressDialogForCallingAlert(SweetAlertDialog sweetAlertDialog) {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);
            sweetAlertDialog.setCustomImage(R.drawable.ic_action_ic_action_call);
            sweetAlertDialog.setTitleText("কল করুন ");
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setCancelText("না");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setConfirmText("হাঁ");
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCanceledOnTouchOutside(true);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    closeDialog();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    Intent intent = new Intent
                            (Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                    context.startActivity(intent);
                    closeDialog();
                }
            });
            sweetAlertDialog.show();
        }
    }

    private void setProgressDialogForLoadingAlert(SweetAlertDialog sweetAlertDialog) {
        setProgressDialogForSearchingAlert(sweetAlertDialog);
        sweetAlertDialog.setCancelable(true);
    }

    private void setProgressDialogForExitingAlert(SweetAlertDialog sweetAlertDialog) {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setTitleText("প্রস্থান");
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.setCancelText("না");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setConfirmText("হাঁ");
            sweetAlertDialog.setConfirmClickListener
                    (new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            Activity activity = (Activity) context;
                            activity.finish();
                            closeDialog();
                        }
                    });
            sweetAlertDialog.setCancelClickListener
                    (new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            closeDialog();
                        }
                    });
            sweetAlertDialog.show();
        }
    }

    private void setProgressDialogForSearchingAlert(SweetAlertDialog sweetAlertDialog) {

        if (sweetAlertDialog != null) {
            sweetAlertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            sweetAlertDialog.getProgressHelper()
                    .setBarColor(context.getResources().getColor(R.color.colorAccent));
            sweetAlertDialog.setTitleText(msg);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
            Log.e(getClass().getName(), "Dialog is shown");

        }

    }

    public void closeDialog() {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
            sweetAlertDialog = null;
        }
    }

    public boolean isShowing() {
        try {
            return sweetAlertDialog.isShowing();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
