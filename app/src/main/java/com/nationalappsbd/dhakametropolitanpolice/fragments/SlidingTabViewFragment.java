package com.nationalappsbd.dhakametropolitanpolice.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.adapters.fragmentAdapters.FragmentAdapter;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPUnitsTable;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;
import com.nationalappsbd.dhakametropolitanpolice.tabs.SlidingTabLayout;

import java.util.ArrayList;

/**
 * Created by shuvojit on 5/7/15.
 */
public class SlidingTabViewFragment extends Fragment implements Initializer {


    private String FRAGMENT_SHOW_TYPE = null;
    private Context context;
    private ArrayList<Fragment> fragmentArrayList;
    private View fragmentView;
    private SlidingTabLayout slidingTabLayout;
    private FragmentAdapter fragmentAdapter;
    private ViewPager viewPager;
    private String[] viewPagerNames;


    public static Fragment getNewInstance(final String MENU_NAME) {
        Bundle bundle = new Bundle();
        bundle.putString("FRAGMENT_SHOW_TYPE", MENU_NAME);
        SlidingTabViewFragment slidingTabViewFragment = new SlidingTabViewFragment();
        slidingTabViewFragment.setArguments(bundle);
        return slidingTabViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            context = getActivity();
            Bundle bundle = getArguments();
            FRAGMENT_SHOW_TYPE = bundle.getString("FRAGMENT_SHOW_TYPE");
            switch (FRAGMENT_SHOW_TYPE) {
                case "Main Page":
                    setFragmentForMainPage();
                    break;
                case "About DMP":
                    setFragmentsForAboutDMP();
                    break;
                case "DMP Units":
                    setFragmentForDmpUnits();
                    break;
                case "DMP Commissioner":
                    setFragmentsForDmpCommissiner();
                    break;
                case "Crime Info":
                    setFragmentForCrimeINfo();
                    break;
            }
        }
    }

    private void setFragmentForMainPage() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = HistoryFragment.getNewInstance();
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(5, "former commissioners");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.mainPage);
    }

    private void setFragmentForCrimeINfo() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(4, "most wanted criminals");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(4, "undefined dead bodies");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.crimeInfo);

    }

    private void setFragmentsForDmpCommissiner() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(3, "message");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(3, "biography");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.dmpComissioner);
    }

    private void setFragmentForDmpUnits() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        viewPagerNames = context.getResources().getStringArray(R.array.dmpUnits);
        //Log.e(getClass().getName(), String.valueOf(viewPagerNames.length));
        for (int i = 1; i < viewPagerNames.length; i++) {
            DMPUnitsTable dmpUnitsTableData = DMPUnitsTable.getDMUnitsTableData(i);
            //Log.e(getClass().getName(), viewPagerNames[i]);
            if (dmpUnitsTableData != null) {
                fragment = DMPNecessaryInfoFragment.getNewInstance(2, dmpUnitsTableData);
                fragmentArrayList.add(fragment);
                //Log.e(getClass().getName(),dmpUnitsTableData.getDetails());
            }
        }

    }

    private void setFragmentsForAboutDMP() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "DMP SERVICES");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "New Initiatives");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "DMP Partners");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.aboutDMP);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = null;

        if (savedInstanceState == null) {
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fragmentView = inflater.inflate(R.layout.view_pager_includer_fragment_layout,
                    container, false);
            initialize();
            setFragmentAdapter();
        }

        return fragmentView;
    }

    private void setFragmentAdapter() {
        if (viewPager != null && fragmentAdapter != null) {
            viewPager.setAdapter(fragmentAdapter);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setViewPager(viewPager);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setSelectedIndicatorColors(getResources()
                    .getColor(R.color.colorAccent));
        }
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            viewPager = (ViewPager) fragmentView.findViewById(R.id.scroll_view_pager);
            slidingTabLayout = (SlidingTabLayout) fragmentView.findViewById(R.id.slidingTab);
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null && fragmentArrayList != null && viewPagerNames != null) {
                fragmentAdapter = new FragmentAdapter(fragmentManager,
                        fragmentArrayList, viewPagerNames);
            }
        }
    }
}
