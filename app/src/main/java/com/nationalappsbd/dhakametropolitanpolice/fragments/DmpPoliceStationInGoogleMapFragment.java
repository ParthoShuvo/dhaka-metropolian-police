package com.nationalappsbd.dhakametropolitanpolice.fragments;

/**
 * Created by shuvojit on 5/8/15.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.activities.PoliceStationInfoActivity;
import com.nationalappsbd.dhakametropolitanpolice.adapters.googleMapInfoWindowAdapters.InfoWindowAdapterForEachSpot;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.GoogleMapClient;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.ArrayList;
import java.util.List;

public class DmpPoliceStationInGoogleMapFragment extends Fragment implements Initializer, GoogleMapClient, GoogleMap.OnInfoWindowClickListener {


    /**
     * Created by Shuvojit Saha Shuvo on 1/26/2015.
     */

    private MapFragment mapFragment;
    private android.app.FragmentManager fragmentManager;
    private GoogleMap googleMap;
    private Bundle bundle;
    private double googleMapCameraZoomLatitude;
    private double googleMapCameraZoomLongtitude;
    private Context context;
    private View fragmentView;
    private UiSettings googleMapUiSettings;
    private ArrayList<Marker> markers;
    private float cameraZoom;
    private String mapLoaderActivity;
    private List<PoliceThanaTable> policeThanaTableList;


    public DmpPoliceStationInGoogleMapFragment() {

    }


    public static DmpPoliceStationInGoogleMapFragment newInstance
            (ArrayList<PoliceThanaTable> policeThanaTableDataList,
             double googleMapCameraZoomLatitude,
             double googleMapCameraZoomLongtitude,
             float cameraZoom,
             String mapLoaderActivity) {
        DmpPoliceStationInGoogleMapFragment dmpPoliceStationInGoogleMapFragment =
                new DmpPoliceStationInGoogleMapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("CycloneCenterArrayList", policeThanaTableDataList);
        bundle.putDouble("GoogleMapCameraZoomLatitude", googleMapCameraZoomLatitude);
        bundle.putDouble("GoogleMapCameraZoomLongtitude", googleMapCameraZoomLongtitude);
        bundle.putFloat("CameraZoom", cameraZoom);
        bundle.putString("Map Loader Activity", mapLoaderActivity);
        dmpPoliceStationInGoogleMapFragment.setArguments(bundle);
        return dmpPoliceStationInGoogleMapFragment;
    }

    public static DmpPoliceStationInGoogleMapFragment newInstance(
            ArrayList<PoliceThanaTable> policeThanaTableDataList,
            double googleMapCameraZoomLatitude,
            double googleMapCameraZoomLongtitude,
            float cameraZoom) {
        DmpPoliceStationInGoogleMapFragment dmpPoliceStationInGoogleMapFragment =
                new DmpPoliceStationInGoogleMapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("CycloneCenterArrayList", policeThanaTableDataList);
        bundle.putDouble("GoogleMapCameraZoomLatitude", googleMapCameraZoomLatitude);
        bundle.putDouble("GoogleMapCameraZoomLongtitude", googleMapCameraZoomLongtitude);
        bundle.putFloat("CameraZoom", cameraZoom);
        dmpPoliceStationInGoogleMapFragment.setArguments(bundle);
        return dmpPoliceStationInGoogleMapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        policeThanaTableList = (ArrayList<PoliceThanaTable>) bundle.
                getSerializable("CycloneCenterArrayList");
        googleMapCameraZoomLatitude = bundle.getDouble("GoogleMapCameraZoomLatitude");
        googleMapCameraZoomLongtitude = bundle.getDouble("GoogleMapCameraZoomLongtitude");
        cameraZoom = bundle.getFloat("CameraZoom");
        mapLoaderActivity = bundle.getString("Map Loader Activity");
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentView = inflater.inflate(R.layout.police_stations_in_google_map, container,
                false);
        initialize();
        setGoogleMapZoom(googleMapCameraZoomLatitude, googleMapCameraZoomLongtitude, cameraZoom);
        addMarkerOnGoogleMap();
        setGoogleMapUiSettings();
        googleMap.setInfoWindowAdapter(new InfoWindowAdapterForEachSpot(context));
        googleMap.setOnInfoWindowClickListener(this);

        return fragmentView;

    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            fragmentManager = getActivity().getFragmentManager();
            mapFragment = (MapFragment) fragmentManager
                    .findFragmentById(R.id.googleMap);
            googleMap = mapFragment.getMap();
            googleMapUiSettings = googleMap.getUiSettings();
            markers = new ArrayList<Marker>();
            Log.e(getClass().getName(), "Google Map has been added");

        }

    }

    @Override
    public void setGoogleMapZoom(double latitude, double longtitude, float cameraZoom) {
        if (googleMap != null) {
            LatLng Bangladesh = new LatLng(latitude, longtitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Bangladesh, cameraZoom));

        }
    }

    @Override
    public void setMarkerColor(Marker marker) {
        if (marker != null) {
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic__app_launcher));
        }
    }

    @Override
    public void addMarkerOnGoogleMap() {
        if (policeThanaTableList != null && googleMap != null && markers != null) {
            LatLng policeStationLatlong = null;
            MarkerOptions markerOptions = null;
            Marker mapMarker = null;
            PoliceThanaTable policeThanaData = null;
            double latitude;
            double longtitude;
            String shelterName;
            Log.e(getClass().getName(), policeThanaTableList.size() + "");
            for (int i = 0; i < policeThanaTableList.size(); i++) {
                policeThanaData = policeThanaTableList.get(i);
                latitude = policeThanaData.getLatitude();
                longtitude = policeThanaData.getLongitude();
                shelterName = policeThanaData.getThanaName();
                policeStationLatlong = new LatLng(latitude, longtitude);
                markerOptions = new MarkerOptions().position(policeStationLatlong).
                        title(shelterName);
                mapMarker = googleMap.addMarker(markerOptions);
                setMarkerColor(mapMarker);
                markers.add(mapMarker);

            }
        }

    }

    public void setGoogleMapUiSettings() {
        if (googleMapUiSettings != null) {
            googleMapUiSettings.setScrollGesturesEnabled(true);
            googleMapUiSettings.setZoomControlsEnabled(true);
            googleMapUiSettings.setMapToolbarEnabled(false);
            googleMapUiSettings.setCompassEnabled(true);

        }
    }


    @Override
    public void onDestroy() {
        try {
            fragmentManager.beginTransaction().remove(mapFragment).commit();
            //remove(mapFragment).commit();
            googleMap = null;
            mapFragment = null;
            fragmentManager = null;
            fragmentView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroyView();

    }


    @Override
    public void onInfoWindowClick(Marker marker) {

        if (mapLoaderActivity == null) {
            getMarkerDetailsFromDatabase(marker);
        }
    }

    private void getMarkerDetailsFromDatabase(Marker marker) {
        if (marker != null) {
            ArrayList<PoliceThanaTable> policeThanaTableArrayList = (ArrayList<PoliceThanaTable>)
                    PoliceThanaTable.getAllPoliceThanaList(marker.getTitle());

            if (policeThanaTableArrayList != null && policeThanaTableArrayList.size() > 0) {
                Intent intent = new Intent(context, PoliceStationInfoActivity.class);
                intent.putExtra("Police Thana Info", policeThanaTableArrayList);
                intent.putExtra("ACTION_BAR_NAME", "পুলিশ থানাসমুহ");
                startActivity(intent);
            }
        }
    }


}

