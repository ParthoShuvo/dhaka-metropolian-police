package com.nationalappsbd.dhakametropolitanpolice.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPHistoryTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ImageGalleryTable;
import com.nationalappsbd.dhakametropolitanpolice.infos.ConnectivityInfo;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.List;

/**
 * Created by shuvojit on 5/9/15.
 */
public class HistoryFragment extends Fragment implements Initializer {

    private Context context;
    private View fragmentView;
    private TextView textView;
    private List<ImageGalleryTable> imageGalleryTableList;
    private int[] imageResources;
    private String[] dmpImagesDetails;

    public HistoryFragment() {

    }

    public static HistoryFragment getNewInstance() {
        return new HistoryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        LayoutInflater layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fragmentView = layoutInflater.inflate(R.layout.history_fragment_layout, null, false);
        initialize();
        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            imageGalleryTableList = ImageGalleryTable.getImageGalleryTableDataList();
            textView = (TextView) fragmentView.findViewById(R.id.dmp_text_info);
            DMPHistoryTable dmpHistoryTable = DMPHistoryTable.load(DMPHistoryTable.class, 1);
            textView.setText(dmpHistoryTable.getHistory().trim());
            TypedArray typedArray = context.getResources().obtainTypedArray(R.array.dmpImags);
            if (typedArray != null && typedArray.length() > 0) {
                imageResources = new int[typedArray.length()];
                for (int i = 0; i < typedArray.length(); i++) {
                    imageResources[i] = typedArray.getResourceId(i, 0);
                }
                typedArray.recycle();
            }
            dmpImagesDetails = context.getResources().getStringArray(R.array.dmpImagesdetails);
            setImageSlider();
        }
    }

    private void setImageSlider() {
        SliderLayout imageSliderLayout = (SliderLayout) fragmentView
                .findViewById(R.id.image_slider);
        Log.e(getClass().getName(), imageResources.length + " " + dmpImagesDetails.length);
        for (int i = 0; i < 30; i++) {
            TextSliderView textSliderView = new TextSliderView(context);
            textSliderView
                    .description(imageGalleryTableList.get(i).getImageDescription())
                    .image(imageResources[i])
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            imageSliderLayout.addSlider(textSliderView);
        }
        if (ConnectivityInfo.isInternetConnectionOn(context)) {
            for (int i = 30; i < imageGalleryTableList.size(); i++) {
                ImageGalleryTable imageGalleryTable = imageGalleryTableList.get(i);
                if (!imageGalleryTable.getImageLink().equals("") ||
                        imageGalleryTable.getImageLink() != null) {
                    TextSliderView textSliderView = new TextSliderView(context);
                    textSliderView
                            .description(imageGalleryTable.getImageDescription())
                            .image(imageGalleryTable.getImageLink())
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    imageSliderLayout.addSlider(textSliderView);
                }
            }
        }


        imageSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        imageSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        imageSliderLayout.setCustomAnimation(new DescriptionAnimation());
        imageSliderLayout.setDuration(5000);
    }

}

