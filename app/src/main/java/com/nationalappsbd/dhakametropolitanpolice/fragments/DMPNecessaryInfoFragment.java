package com.nationalappsbd.dhakametropolitanpolice.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.activities.PoliceStationInfoActivity;
import com.nationalappsbd.dhakametropolitanpolice.adapters.gridViewAdapters.DmpInfoGridViewAdapter;
import com.nationalappsbd.dhakametropolitanpolice.adapters.listViewAdapters.DMPInfoListViewAdapter;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.CommissionerListTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ComplaintTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPPressTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPServicesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPUnitsTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMP_PartersTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DmpCommissionerTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.MostWantedCriminalsTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.NewInitiativesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.UndefinedDeadBodiesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.WeRememberTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.infos.ConnectivityInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */
public class DMPNecessaryInfoFragment extends Fragment {

    private ListView listView;
    private Context context;
    private int FRAGMENT_SHOW_TYPE;
    private LayoutInflater layoutInflater;
    private String FRAGMENT_NAME;
    private GridView gridView;
    private DMPUnitsTable dmpUnitsTableData;
    private DmpInfoGridViewAdapter dmpInfoGridViewAdapter;


    public DMPNecessaryInfoFragment() {

    }

    public static Fragment getNewInstance(final int FRAGMENT_SHOW_TYPE,
                                          final String Fragment_Name) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putString("FRAGMENT_NAME", Fragment_Name);
        DMPNecessaryInfoFragment dmpNecessaryInfoFragment = new DMPNecessaryInfoFragment();
        dmpNecessaryInfoFragment.setArguments(bundle);
        return dmpNecessaryInfoFragment;
    }

    public static Fragment getNewInstance(final int FRAGMENT_SHOW_TYPE,
                                          final DMPUnitsTable dmpUnitsTableData) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putSerializable("DMP UNIT", dmpUnitsTableData);
        DMPNecessaryInfoFragment dmpNecessaryInfoFragment = new DMPNecessaryInfoFragment();
        dmpNecessaryInfoFragment.setArguments(bundle);
        return dmpNecessaryInfoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Bundle bundle = getArguments();
        FRAGMENT_SHOW_TYPE = bundle.getInt("FRAGMENT_SHOW_TYPE");
        FRAGMENT_NAME = bundle.getString("FRAGMENT_NAME");
        dmpUnitsTableData = (DMPUnitsTable) bundle.getSerializable("DMP UNIT");

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = null;

        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
                switch (FRAGMENT_NAME) {
                    case "DMP Partners":
                        fragmentView = layoutInflater.inflate(R.layout.dmp_info_grid_view_layout,
                                container, false);
                        break;
                    default:
                        fragmentView = layoutInflater.inflate(R.layout.dmp_info_list_view_layout,
                                container, false);
                        break;
                }
                break;
            case 2:
                fragmentView = layoutInflater.inflate(R.layout.dmp_necessary_info_layout,
                        null, false);

                break;
            case 3:
                fragmentView = layoutInflater.inflate
                        (R.layout.vertically_image_with_text_layout, null, false);
                break;
            case 4:
            case 5:
                fragmentView = layoutInflater.inflate(R.layout.dmp_info_grid_view_layout,
                        container, false);
                break;
            case 6:
                fragmentView = layoutInflater.inflate(R.layout.dmp_info_list_view_layout,
                        container, false);
                break;
            case 7:
                fragmentView = layoutInflater.inflate(R.layout.list_view_with_text_layout,
                        container, false);
                break;


        }
        setFragmentLayoutElements(fragmentView);
        return fragmentView;
    }

    private void setFragmentLayoutElements(View fragmentView) {
        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
                switch (FRAGMENT_NAME) {
                    case "DMP Partners":
                        gridView = (GridView) fragmentView.findViewById(R.id.gridView);
                        break;
                    default:
                        listView = (ListView) fragmentView.findViewById(R.id.dmp_info_list_view);
                        break;
                }
                setListFragmentView(fragmentView);
                break;
            case 2:
                setTextFragmentView(fragmentView);
                break;
            case 3:
                setVerticallyImageRextFragmnetView(fragmentView);
                break;
            case 4:
            case 5:
                gridView = (GridView) fragmentView.findViewById(R.id.gridView);
                setListFragmentView(fragmentView);
                break;
            case 6:
                listView = (ListView) fragmentView.findViewById(R.id.dmp_info_list_view);
                setListFragmentView(fragmentView);
                break;
            case 7:
                listView = (ListView) fragmentView.findViewById(R.id.dmp_info_list_view);
                setListFragmentView(fragmentView);
                break;

        }
    }

    private void setVerticallyImageRextFragmnetView(View fragmentView) {
        ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);
        TextView txtDMPINfo = (TextView) fragmentView.findViewById(R.id.dmp_text_info);
        DmpCommissionerTable dmpCommissioner = DmpCommissionerTable.getDmpCommissioner();
        String details = null;
        if (dmpCommissioner != null) {

            Picasso.with(context)
                    .load(dmpCommissioner
                            .getImageLink())
                    .placeholder(R.drawable.dmp_comissioner)
                    .error(R.drawable.dmp_comissioner)
                    .into(imageView);
            switch (FRAGMENT_NAME) {
                case "message":
                    details = dmpCommissioner.getMessage().toString().trim();
                    txtDMPINfo.setText(details);
                    break;
                case "biography":
                    details = dmpCommissioner.getBiography().toString().trim();
                    txtDMPINfo.setText(details);
                    break;
            }

        }

    }

    private void setTextFragmentView(View fragmentView) {
        TextView txtDMPInfo = (TextView) fragmentView.findViewById(R.id.txt_dmp_info);
        if (dmpUnitsTableData != null) {
            String details = dmpUnitsTableData.getDetails().toString().trim();
            txtDMPInfo.setText(details);
            Log.e(getClass().getName(), "found");
        }
    }

    private void setListFragmentView(View fragmentView) {
        DMPInfoListViewAdapter dmpInfoListViewAdapter = null;
        switch (FRAGMENT_NAME) {
            case "DMP SERVICES":
                /*String categories[] = context.getResources()
                        .getStringArray(R.array.serviceCateagory);
                String details[] = context.getResources()
                        .getStringArray(R.array.serviceDetails);*/
                List<DMPServicesTable> dmpServicesTableList = DMPServicesTable
                        .getdmpServicesTableList();
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter
                        (context, 1, dmpServicesTableList);
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                }

                break;
            case "New Initiatives":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 2,
                        NewInitiativesTable.getNewInitiativesTableDataList());
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "DMP Partners":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 3,
                        DMP_PartersTable.getPartersDataList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    gridView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "We Remember":
                final List<WeRememberTable> weRememberTables = WeRememberTable
                        .getWeRememberTableDataList();
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 4,
                        weRememberTables);
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            WeRememberTable weRememberTable = weRememberTables.get(position);
                            if (weRememberTable != null) {
                                Intent intent = new Intent(context,
                                        PoliceStationInfoActivity.class);
                                intent.putExtra("We Remember", weRememberTable);
                                intent.putExtra("Position", position);
                                intent.putExtra("ACTION_BAR_NAME", "আমরা স্মরণ করি");
                                context.startActivity(intent);
                            }
                        }
                    });
                }
                break;
            case "most wanted criminals":
                if (MostWantedCriminalsTable.getAlMostWantedCriminals() != null) {
                    dmpInfoGridViewAdapter = new
                            DmpInfoGridViewAdapter(context, 1,
                            MostWantedCriminalsTable.getAlMostWantedCriminals());
                    if (dmpInfoGridViewAdapter != null) {
                        gridView.setAdapter(dmpInfoGridViewAdapter);
                        Log.e(getClass().getName(), "not null");
                    }
                }
                break;
            case "undefined dead bodies":
                dmpInfoGridViewAdapter = new
                        DmpInfoGridViewAdapter(context, 2,
                        UndefinedDeadBodiesTable.getAllUndefinedDeadBodies());
                gridView.setNumColumns(1);
                if (dmpInfoGridViewAdapter != null) {
                    gridView.setAdapter(dmpInfoGridViewAdapter);
                    Log.e(getClass().getName(), "not null");
                }
                break;
            case "former commissioners":
                dmpInfoGridViewAdapter = new
                        DmpInfoGridViewAdapter(context, 3,
                        CommissionerListTable.getCommissionerList());
                if (dmpInfoGridViewAdapter != null) {
                    gridView.setAdapter(dmpInfoGridViewAdapter);
                    Log.e(getClass().getName(), "not null");
                }
                break;
            case "DMP Press":
                final List<DMPPressTable> dmpPressTableList = DMPPressTable.getDmpPressList();
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 7,
                        dmpPressTableList);
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                View view, int position, long id) {
                            DMPPressTable dmpPressTable = dmpPressTableList.get(position);
                            if (dmpPressTableList != null) {
                                Intent intent = new Intent(context,
                                        PoliceStationInfoActivity.class);
                                intent.putExtra("DMP Press", dmpPressTable);
                                intent.putExtra("ACTION_BAR_NAME", "সংবাদ বিজ্ঞপ্তি");
                                context.startActivity(intent);
                            }
                        }
                    });
                }
                break;
            case "Complaint":
                final TextView textView = (TextView) fragmentView
                        .findViewById(R.id.txt_police_control_room_email);
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 8,
                        ComplaintTable.getComplaintPhNumber());
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String number = ComplaintTable.getComplaintPhNumber().get(position)
                                .getPhNumber();
                        if (number.charAt(0) != '0') {
                            number = "02" + number;
                        }
                        String msg = "পুলিশ কন্ট্রোল রুম এ কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে";
                        UserNotifiedDialog userNotifiedDialog = new UserNotifiedDialog
                                (context, "Calling Alert", msg, number);
                        userNotifiedDialog.showDialog();
                    }
                });
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                            Toast.makeText(context,
                                    "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                        } else {
                            Intent testIntent = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri.parse
                                    ("mailto:?subject=" + "Police Control Room" +
                                            "&body=" + "" + "&to=" + textView.getText().toString());
                            testIntent.setData(data);
                            startActivity(testIntent);
                        }
                    }
                });
        }

    }

}
