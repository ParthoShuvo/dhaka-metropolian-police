package com.nationalappsbd.dhakametropolitanpolice.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.activities.PoliceStationInfoActivity;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuvojit on 5/10/15.
 */
public class PoliceStationSelectionFragment extends Fragment implements View.OnClickListener,
        Initializer {

    private Context context;
    private View fragmentView;
    private Spinner spnPoliceStationName;
    private Button btnPoliceStationSearch;
    private List<String> policeStationList;
    private ArrayAdapter<String> policeStationNameListAdapter;
    private String policeStation;
    private ArrayList<PoliceThanaTable> policeStationInfoArrayList;
    private AdapterView.OnItemSelectedListener spnPoliceStationNameSelectorListener = new
            AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    policeStation = policeStationList.get(position);
                    Log.e(getClass().getName(), policeStation);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };

    public PoliceStationSelectionFragment() {

    }

    public static PoliceStationSelectionFragment getNewInstance() {
        return new PoliceStationSelectionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentView = inflater.
                inflate(R.layout.police_station_selection_fragment,
                        container, false);
        initialize();
        setArrayAdapter();
        setSpinnerListener();
        if (btnPoliceStationSearch != null) {
            btnPoliceStationSearch.setOnClickListener(this);
        }

        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            context = getActivity();
            spnPoliceStationName = (Spinner) fragmentView.findViewById(R.id.spinner_police_name);
            btnPoliceStationSearch = (Button) fragmentView.findViewById
                    (R.id.btn_police_station_search);
            initializeArrayAdapterForSpinner();

        }
    }

    private void setSpinnerListener() {

        spnPoliceStationName.setOnItemSelectedListener(spnPoliceStationNameSelectorListener);


    }

    private void setArrayAdapter() {
        if (policeStationNameListAdapter != null) {
            Log.e(getClass().getName(), "zilla name list has been added");
            spnPoliceStationName.setAdapter(policeStationNameListAdapter);

        }
    }

    private void initializeArrayAdapterForSpinner() {

        policeStationList = PoliceThanaTable.getAllPoliceThanaNameList();
        if (policeStationList != null) {

            policeStationNameListAdapter = new ArrayAdapter<String>(context,
                    R.layout.spinner_text_view_layout, policeStationList);
            policeStationNameListAdapter.
                    setDropDownViewResource(android.R.layout.simple_list_item_single_choice);


        }
    }

    @Override
    public void onClick(View v) {
        Log.e(getClass().getName(), "Center is Seraching");

        searchPoliceStation();

    }

    private void searchPoliceStation() {
        if (policeStation != null) {
            policeStationInfoArrayList = (ArrayList<PoliceThanaTable>) PoliceThanaTable
                    .getAllPoliceThanaList(policeStation);
            if (policeStationInfoArrayList != null && policeStationInfoArrayList.size() > 0) {
                Intent intent = new Intent(context, PoliceStationInfoActivity.class);
                intent.putExtra("Police Thana Info", policeStationInfoArrayList);
                intent.putExtra("ACTION_BAR_NAME", "পুলিশ থানাসমুহ");
                startActivity(intent);
            }

        }
    }


}
