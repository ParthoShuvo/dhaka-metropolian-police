package com.nationalappsbd.dhakametropolitanpolice.backgroundServices;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.nationalappsbd.dhakametropolitanpolice.R;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.CommissionerListTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ComplaintTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPHistoryTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPPressTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPServicesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMPUnitsTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DMP_PartersTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.DmpCommissionerTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ImageGalleryTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.ImportantNumbersTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.MostWantedCriminalsTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.NewInitiativesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceClearanceCertificateTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.PoliceThanaTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.UndefinedDeadBodiesTable;
import com.nationalappsbd.dhakametropolitanpolice.databaseTables.WeRememberTable;
import com.nationalappsbd.dhakametropolitanpolice.dialogs.UserNotifiedDialog;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.GalleryImageContents;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.GalleryImages;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.Menu;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.MenuContents;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.SubMenu;
import com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson.SubMenuContents;
import com.nationalappsbd.dhakametropolitanpolice.infos.DMPStationInfo;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonLinks;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.MenuIDsInterface;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.SubMenuNamesInterface;
import com.nationalappsbd.dhakametropolitanpolice.webservices.JsonPostClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by shuvojit on 5/11/15.
 */
public class BackGroundAsynTask extends AsyncTask<Void, Void, Boolean> implements JsonLinks,
        JsonFields, MenuIDsInterface, SubMenuNamesInterface {


    private final String APP_ID = "191";
    private boolean updated = false;
    private List<DMPStationInfo> policeStationInfoList;
    private Context context;

    public BackGroundAsynTask() {


    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {
            policeStationInfoList = new ArrayList<DMPStationInfo>();
            getMenuUpdate();
            getSubMenuUpdate();
            getImageUpdate();
            context = UserNotifiedDialog.context;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (updated) {
            return true;
        }
        return false;
    }

    private void getSubMenuUpdate() {
        String menuID[] = new String[]{ABOUT_DMP_ID, HOME_PAGE_ID, DMP_COMMISSIONER_ID,
                DMP_INTERNAL_ORGANIZATIONS, CRIME_INFO, POLICE_STATIONS};
        try {
            for (int i = 0; i < menuID.length; i++) {
                JSONObject jsonput = new JSONObject();
                jsonput.put(JsonFields.app_id, APP_ID);
                jsonput.put(JsonFields.menuId, menuID[i]);
                JSONObject jsonsend = JsonPostClient.SendHttpPost(JsonLinks.submenuList,
                        jsonput);
                if (jsonsend != null) {
                    String jsonString = jsonsend.toString();
                    Gson gson = new Gson();
                    SubMenu subMenu = gson.fromJson(jsonString, SubMenu.class);
                    if (subMenu != null) {
                        ArrayList<SubMenuContents> subMenuContentsArrayList = subMenu
                                .getSubMenuContentsArrayList();
                        getAllSubMenusElements(subMenuContentsArrayList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllSubMenusElements(ArrayList<SubMenuContents>
                                                subMenuContentsArrayList) {
        if (subMenuContentsArrayList != null
                && subMenuContentsArrayList.size() > 0) {
            for (int i = 0; i < subMenuContentsArrayList.size(); i++) {
                SubMenuContents subMenuContents = subMenuContentsArrayList.get(i);
                if (subMenuContents != null) {
                    String subMenuName = subMenuContents.getSubMenuName();
                    String contents = subMenuContents.getContent();
                    if (subMenuName != null && subMenuContents != null) {
                       /* Log.e(getClass().getName(), subMenuName);
                        Log.e(getClass().getName(), contents);*/
                        findSubMenuContents(subMenuName, contents);
                    } else {
                        Log.e(getClass().getName(), "Contents are null");
                    }
                }
            }
        }
    }

    private void findSubMenuContents(String subMenuName, String contents) {
        String data = null;
        List<String> dataList = null;
        int size = 0;
        switch (subMenuName) {
            case MESSAGE_OF_COMMISSIONER:
                //dmpCommissioner = new ArrayList<String>();
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPCommissionerMenu(data, 1);
                    Log.e(getClass().getName(), "Commissioner Message Retrieved");
                    updated = true;
                }
                break;
            case BIOGRAPHY_OF_COMMISSIONER:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPCommissionerMenu(data, 2);
                    Log.e(getClass().getName(), "Biography of DMP Commissioner Retrieved");
                    updated = true;
                }
                break;
            case IMAGE_LINK_OF_COMMISSIONER:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPCommissionerMenu(data, 3);
                    Log.e(getClass().getName(), "Image Link of DMP commissioner Retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_1:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 1);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_2:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 2);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_3:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 3);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_4:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 4);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_5:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 5);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_6:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 6);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_7:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 7);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_8:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 8);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_9:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 9);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case NEW_INITIATIVE_10:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateNewInitiativesInfo(data, 10);
                    Log.e(getClass().getName(), "new initiative retrieved");
                    updated = true;
                }
                break;
            case DMP_INTERNAL_ORGANIZATION_1:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPOrganizations(data, 1);
                    Log.e(getClass().getName(), "dmp organizations retrieved");
                    updated = true;
                }
                break;
            case DMP_INTERNAL_ORGANIZATION_2:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPOrganizations(data, 2);
                    Log.e(getClass().getName(), "dmp organizations retrieved");
                    updated = true;
                }
                break;
            case DMP_INTERNAL_ORGANIZATION_3:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPOrganizations(data, 3);
                    Log.e(getClass().getName(), "dmp organizations retrieved");
                    updated = true;
                }
                break;
            case DMP_INTERNAL_ORGANIZATION_4:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPOrganizations(data, 4);
                    Log.e(getClass().getName(), "dmp organizations retrieved");
                    updated = true;
                }
                break;
            case DMP_INTERNAL_ORGANIZATION_5:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPOrganizations(data, 5);
                    Log.e(getClass().getName(), "dmp organizations retrieved");
                    updated = true;
                }
                break;
            case DMP_HISTORY:
                data = decodeString(contents).trim();
                if (data != null && !data.equals("") && !data.equalsIgnoreCase("null")) {
                    updateDMPHistory(data);
                    Log.e(getClass().getName(), "dmp history retrieved");
                    updated = true;
                }
                break;
            case DMP_SERVICES_1:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPServices(dataList, 1);
                    Log.e(getClass().getName(), "DMP Services retrieved");
                    updated = true;
                }
                break;
            case DMP_SERVICES_2:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPServices(dataList, 2);
                    Log.e(getClass().getName(), "DMP Services retrieved");
                    updated = true;
                }
                break;
            case DMP_SERVICES_3:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPServices(dataList, 3);
                    Log.e(getClass().getName(), "DMP Services retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_1:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 1);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_2:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 2);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_3:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 3);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_4:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 4);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_5:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 5);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case DMP_PARTNERS_6:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 6);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_7:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 7);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_8:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 8);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_9:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 9);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_10:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 10);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_11:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 11);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;

            case DMP_PARTNERS_12:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDMPPartners(dataList, 12);
                    Log.e(getClass().getName(), "DMP Partners retrieved");
                    updated = true;
                }
                break;
            case MOST_WANTED_CRIMINALS:
                dataList = decodeStringList(contents);
                size = MostWantedCriminalsTable.getAlMostWantedCriminals().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateMostWantedCrminalList(dataList);
                    updated = true;
                }
                break;
            case UNDEFINED_DEAD_BODIES:
                dataList = decodeStringList(contents);
                size = UndefinedDeadBodiesTable.getAllUndefinedDeadBodies().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateUndefinedDeadBodiesList(dataList);
                    updated = true;
                }
                break;
            case PREVIOUS_DMP_COMMISSIONER:
                dataList = decodeStringList(contents);
                size = CommissionerListTable.getCommissionerList().size();
                if (dataList != null && dataList.size() >= (size * 3)) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateFormerCommissionerList(dataList);
                    updated = true;
                }
                break;
            case POLICE_STATION_1:
            case POLICE_STATION_2:
            case POLICE_STATION_3:
            case POLICE_STATION_4:
            case POLICE_STATION_5:
            case POLICE_STATION_6:
            case POLICE_STATION_7:
            case POLICE_STATION_8:
            case POLICE_STATION_9:
            case POLICE_STATION_10:
            case POLICE_STATION_11:
            case POLICE_STATION_12:
            case POLICE_STATION_13:
            case POLICE_STATION_14:
            case POLICE_STATION_15:
            case POLICE_STATION_16:
            case POLICE_STATION_17:
            case POLICE_STATION_18:
            case POLICE_STATION_19:
            case POLICE_STATION_20:
            case POLICE_STATION_21:
            case POLICE_STATION_22:
            case POLICE_STATION_23:
            case POLICE_STATION_24:
            case POLICE_STATION_25:
            case POLICE_STATION_26:
            case POLICE_STATION_27:
            case POLICE_STATION_28:
            case POLICE_STATION_29:
            case POLICE_STATION_30:
            case POLICE_STATION_31:
            case POLICE_STATION_32:
            case POLICE_STATION_33:
            case POLICE_STATION_34:
            case POLICE_STATION_35:
            case POLICE_STATION_36:
            case POLICE_STATION_37:
            case POLICE_STATION_38:
            case POLICE_STATION_39:
            case POLICE_STATION_40:
            case POLICE_STATION_41:
            case POLICE_STATION_42:
            case POLICE_STATION_43:
            case POLICE_STATION_44:
            case POLICE_STATION_45:
            case POLICE_STATION_46:
            case POLICE_STATION_47:
            case POLICE_STATION_48:
            case POLICE_STATION_49:
                Log.e(getClass().getName(), "Information Found");
                findOutPoliceStationInfo(contents);
                if (policeStationInfoList.size() == 49) {
                    updatePoliceStationList();
                    updated = true;
                }
                break;

        }
    }

    private void updateFormerCommissionerList(List<String> dataList) {
        List<CommissionerListTable> commissionerListTableList =
                CommissionerListTable.getCommissionerList();
        int size = commissionerListTableList.size();
        String name = null;
        String imageLink = null;
        String workingPeriod = null;
        int j = 0;
        Log.e(getClass().getName(), String.valueOf(dataList.size()));
        if (commissionerListTableList != null && commissionerListTableList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 3) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                workingPeriod = dataList.get(i + 1);
                imageLink = dataList.get(i + 2);
                if (i > size * 3) {
                    CommissionerListTable commissionerListTable = new
                            CommissionerListTable(name, imageLink, workingPeriod);
                    Log.e(getClass().getName(), "Undefined Bodies item added");
                } else {
                    j++;
                    CommissionerListTable commissionerListTable =
                            CommissionerListTable.load(CommissionerListTable.class, j);
                    if (commissionerListTable.getName() != null &&
                            !commissionerListTable
                                    .getName()
                                    .trim()
                                    .equals(name)) {
                        commissionerListTable.setName(name);
                        Log.e(getClass().getName(), "Updated Commissioner list table");
                    }

                    if (commissionerListTable.getWorkingPeriod() != null &&
                            !commissionerListTable
                                    .getWorkingPeriod()
                                    .trim()
                                    .equals(workingPeriod)) {
                        commissionerListTable.setWorkingPeriod(workingPeriod);
                        Log.e(getClass().getName(), "Updated Commissioner list table");
                    }

                    if (commissionerListTable.getImageLink() != null &&
                            !commissionerListTable
                                    .getImageLink()
                                    .trim()
                                    .equals(imageLink)) {
                        commissionerListTable.setImageLink(imageLink);
                        Log.e(getClass().getName(), "Updated Commissioner list table");
                    }
                    commissionerListTable.save();

                }
            }
        }
    }

    private void updateUndefinedDeadBodiesList(List<String> dataList) {
        List<UndefinedDeadBodiesTable> undefinedDeadBodiesTableList =
                UndefinedDeadBodiesTable.getAllUndefinedDeadBodies();
        int size = undefinedDeadBodiesTableList.size();
        String name = null;
        String imageLink = null;
        int j = 0;
        if (undefinedDeadBodiesTableList != null && undefinedDeadBodiesTableList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                imageLink = dataList.get(i + 1);
                if (i > size * 2) {
                    UndefinedDeadBodiesTable undefinedDeadBodiesTable = new
                            UndefinedDeadBodiesTable(name, imageLink);
                    Log.e(getClass().getName(), "Undefined Bodies item added");
                } else {
                    j++;
                    UndefinedDeadBodiesTable undefinedDeadBodiesTable =
                            UndefinedDeadBodiesTable.load(UndefinedDeadBodiesTable.class, j);
                    if (undefinedDeadBodiesTable.getName() != null &&
                            !undefinedDeadBodiesTable
                                    .getName()
                                    .trim()
                                    .equals(name)) {
                        undefinedDeadBodiesTable.setName(name);
                        Log.e(getClass().getName(), "Updated Undefined Bodies list");
                    }
                    if (undefinedDeadBodiesTable.getImageLink() != null &&
                            !undefinedDeadBodiesTable
                                    .getImageLink()
                                    .trim()
                                    .equals(imageLink)) {
                        undefinedDeadBodiesTable.setImageLink(imageLink);
                        Log.e(getClass().getName(), "Updated Undefined Bodies list");
                    }
                    undefinedDeadBodiesTable.save();

                }
            }
        }
    }

    private void updateMostWantedCrminalList(List<String> dataList) {
        List<MostWantedCriminalsTable> mostWantedCriminalsTableList =
                MostWantedCriminalsTable.getAlMostWantedCriminals();
        int size = mostWantedCriminalsTableList.size();
        String name = null;
        String imageLink = null;
        int j = 0;
        if (mostWantedCriminalsTableList != null && mostWantedCriminalsTableList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                imageLink = dataList.get(i + 1);
                if (i > size * 2) {
                    MostWantedCriminalsTable mostWantedCriminalsTable = new
                            MostWantedCriminalsTable(name, imageLink);
                    Log.e(getClass().getName(), "Most wanted item added");
                } else {
                    j++;
                    MostWantedCriminalsTable mostWantedCriminalsTable =
                            MostWantedCriminalsTable.load(MostWantedCriminalsTable.class, j);
                    if (mostWantedCriminalsTable.getName() != null &&
                            !mostWantedCriminalsTable
                                    .getName()
                                    .trim()
                                    .equals(name)) {
                        mostWantedCriminalsTable.setName(name);
                        Log.e(getClass().getName(), "Updated Most wanted list");
                    }
                    if (mostWantedCriminalsTable.getImageLink() != null &&
                            !mostWantedCriminalsTable
                                    .getImageLink()
                                    .trim()
                                    .equals(imageLink)) {
                        mostWantedCriminalsTable.setImageLink(imageLink);
                        Log.e(getClass().getName(), "Updated Most wanted list");
                    }
                    mostWantedCriminalsTable.save();

                }
            }
        }
    }

    private void updateDMPPartners(List<String> dataList, int id) {
        DMP_PartersTable dmpPartersTable = DMP_PartersTable
                .load(DMP_PartersTable.class, id);
        String partnerName = dataList.get(0);
        String imageLink = dataList.get(1);
        if (dmpPartersTable.getParterName() != null && !dmpPartersTable
                .getParterName()
                .trim()
                .equals(partnerName)) {
            dmpPartersTable.setParterName(partnerName);
            Log.e(getClass().getName(), "DMP Partners updates");
        }
        if (dmpPartersTable.getImageLink() != null && !dmpPartersTable
                .getImageLink()
                .trim()
                .equals(imageLink)) {
            dmpPartersTable.setParterName(imageLink);
            Log.e(getClass().getName(), "DMP Partners updates");
        }
        dmpPartersTable.save();

    }

    private void updateDMPServices(List<String> dataList, int id) {
        DMPServicesTable dmpServicesTable = DMPServicesTable.load(DMPServicesTable.class, id);
        String serviceName = dataList.get(0);
        String serviceDetails = dataList.get(1);
        if (dmpServicesTable.getServiceName() != null && !dmpServicesTable
                .getServiceName()
                .trim()
                .equals(serviceName)) {
            dmpServicesTable.setServiceName(serviceName);
            Log.e(getClass().getName(), " Service has been updated");
        }
        if (dmpServicesTable.getDetails() != null && !dmpServicesTable
                .getDetails()
                .trim()
                .equals(serviceDetails)) {
            dmpServicesTable.setDetails(serviceDetails);
            Log.e(getClass().getName(), " Service has been updated");
        }
        dmpServicesTable.save();

    }

    private List<String> decodeStringList(String content) {
        List<String> stringList = new ArrayList<String>();
        int count = 0;
        Boolean skip = true;
        char character;
        StringBuffer info = null;
        //Log.e(getClass().getName(), content);
        for (int i = 0; i < content.length(); i++) {
            character = content.charAt(i);
            if (character == '<' || character == '&') {
                skip = true;
                continue;
            } else if (character == '>') {
                skip = false;
                if (info != null && !info.toString().trim().equals("")) {
                    Log.e(getClass().getName(), count + " " + info.toString());
                    stringList.add(info.toString().trim());
                    //Log.e(getClass().getName(), "Gone");
                }
                count++;
                info = new StringBuffer();
                info.append("");
                continue;
            } else if (character == ';') {
                skip = false;
                continue;
            }
            if (!skip) {
                if (character == '|') {
                    info.append(Character.toString('।'));
                } else {
                    info.append(Character.toString(character));
                }
            }
        }
        return stringList;
    }


    private void updateDMPHistory(String data) {
        DMPHistoryTable dmpHistoryTable = DMPHistoryTable.load(DMPHistoryTable.class, 1);
        if (dmpHistoryTable != null && !dmpHistoryTable
                .getHistory()
                .trim()
                .equals(data)) {
            dmpHistoryTable.setHistory(data);
        }
        dmpHistoryTable.save();
    }

    private void updateDMPOrganizations(String data, int id) {
        DMPUnitsTable dmpUnitsTable = DMPUnitsTable.load(DMPUnitsTable.class, id);
        if (dmpUnitsTable != null && !dmpUnitsTable
                .getDetails()
                .trim()
                .equals(data)) {
            dmpUnitsTable.setDetails(data);
            Log.e(getClass().getName(), "dmp organization has been updated");
        }
        dmpUnitsTable.save();
    }

    private void getImageUpdate() {
        try {
            JSONObject jsonput = new JSONObject();
            JSONObject jsonsend = null;
            jsonput.put(JsonFields.app_id, APP_ID);
            jsonsend = JsonPostClient.SendHttpPost(JsonLinks.getGallery,
                    jsonput);
            if (jsonsend != null) {
                String jsonData = jsonsend.toString();
                //Log.e(getClass().getName(), jsonsend.toString());
                if (jsonData != null && !jsonData.equals("")) {
                    Gson gson = new Gson();
                    GalleryImages galleryImages = gson.fromJson(jsonData, GalleryImages.class);
                    if (galleryImages != null) {
                        ArrayList<GalleryImageContents> galleryImageContentsArrayList =
                                galleryImages.getImageContentsArrayList();
                        if (galleryImageContentsArrayList != null
                                && galleryImageContentsArrayList.size() > 0) {
                            Log.e(getClass().getName(), "Gallery Images Retreived");
                            getAllImageArray(galleryImageContentsArrayList);

                        }
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getAllImageArray(ArrayList<GalleryImageContents>
                                          galleryImageContentsArrayList) {
        List<ImageGalleryTable> imageGalleryTableList = ImageGalleryTable
                .getImageGalleryTableDataList();
        if (imageGalleryTableList != null && imageGalleryTableList.size() > 0) {
            for (int i = 0; i < galleryImageContentsArrayList.size(); i++) {
                GalleryImageContents galleryImageContents =
                        galleryImageContentsArrayList.get(i);
                String active = galleryImageContents.getActive();
                String description = galleryImageContents.getDescription();
                String filePath = galleryImageContents.getImagePath();
                Log.e(getClass().getName(), "Image reterived");
                updated = true;
                if (active.equals("1")) {
                    if (i >= imageGalleryTableList.size()) {
                        ImageGalleryTable imageGalleryTable = new
                                ImageGalleryTable(filePath, description);
                        Log.e(getClass().getName(), "Image Added");

                    } else {
                        ImageGalleryTable imageGalleryTable = ImageGalleryTable
                                .load(ImageGalleryTable.class, imageGalleryTableList.get(i).getId());
                        if (imageGalleryTable != null) {
                            if (imageGalleryTable.getImageDescription() != null &&
                                    !imageGalleryTable.getImageDescription().equals(description)) {
                                imageGalleryTable.setImageDescription(description);
                                Log.e(getClass().getName(), "updated image");
                            }
                            if (imageGalleryTable.getImageLink() != null &&
                                    !imageGalleryTable.getImageLink().equals(filePath)) {
                                imageGalleryTable.setImageLink(filePath);
                                Log.e(getClass().getName(), "updated image");
                            }


                        }
                        imageGalleryTable.save();

                    }
                }

            }
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        SweetAlertDialog sweetAlertDialog = null;

        if (aBoolean) {
            sweetAlertDialog = new SweetAlertDialog(context,
                    SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setTitleText(context.getResources()
                    .getString(R.string.updateSuccessfulNotifier));

        } else {
            sweetAlertDialog = new SweetAlertDialog(context,
                    SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(context.getResources()
                    .getString(R.string.updateFailedNotifier));

        }
        try {
            if (sweetAlertDialog != null) {
                sweetAlertDialog.setConfirmText("বন্ধ করুন");
                sweetAlertDialog.showCancelButton(false);
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener
                        (new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        });
                sweetAlertDialog.show();
            }

        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getMenuUpdate() {

        try {
            JSONObject jsonput = new JSONObject();
            JSONObject jsonsend = null;
            jsonput.put(JsonFields.app_id, "191");
            jsonsend = JsonPostClient.SendHttpPost(JsonLinks.menuList,
                    jsonput);
            if (jsonsend != null) {
                String jsonString = jsonsend.toString();
                if (jsonString != null && !jsonString.equals("")) {
                    Gson gson = new Gson();
                    Menu menu = gson.fromJson(jsonString, Menu.class);
                    if (menu != null) {
                        ArrayList<MenuContents> menuContentsArrayList = menu
                                .getMenuContentsArrayList();
                        if (menuContentsArrayList != null && menuContentsArrayList.size() > 0) {
                            getAllMenuArrayUpdate(menuContentsArrayList);
                        }

                    }

                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getAllMenuArrayUpdate(ArrayList<MenuContents> menuContentsArrayList) {
        for (int i = 0; i < menuContentsArrayList.size(); i++) {
            MenuContents menuContents = menuContentsArrayList.get(i);
            if (menuContents != null) {
                getJsonMenuElements(menuContents);
            }

        }


    }

    private void getJsonMenuElements(MenuContents menuContents) {
        String content = menuContents.getContent();
        String res = null;
        List<String> dataList = null;
        String history = "";
        String menuID = menuContents.getMenuID();
        switch (menuID) {
            case NEWS_UPDATE:
                dataList = decodeStringList(content);
                int size = DMPPressTable.getDmpPressList().size();
                if (dataList != null && dataList.size() >= 3) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    Log.e(getClass().getName(), "Total News is :" + String
                            .valueOf(dataList.size()));
                    updateNews(dataList);
                    updated = true;
                }
                break;
            case WE_REMEMBER_1:
                dataList = decodeStringList(content);
                history = "";
                if (dataList != null && dataList.size() >= 4) {
                    for (int i = 3; i < dataList.size(); i++) {
                        history += dataList.get(i);
                    }
                    updateWeRemember(dataList, history, 1);
                    Log.e(getClass().getName(), "We remember1 found");
                    updated = true;
                }
                break;
            case WE_REMEMBER_2:
                dataList = decodeStringList(content);
                history = "";
                if (dataList != null && dataList.size() >= 4) {
                    for (int i = 3; i < dataList.size(); i++) {
                        history += dataList.get(i);
                    }
                    updateWeRemember(dataList, history, 2);
                    Log.e(getClass().getName(), "We remember2 found");
                    updated = true;
                }
                break;
            case WE_REMEMBER_3:
                dataList = decodeStringList(content);
                history = "";
                if (dataList != null && dataList.size() >= 4) {
                    for (int i = 3; i < dataList.size(); i++) {
                        history += dataList.get(i);
                    }
                    updateWeRemember(dataList, history, 3);
                    Log.e(getClass().getName(), "We remember3 found");
                    updated = true;
                }
                break;
            case COMPLAIN:
                dataList = decodeStringList(content);
                if (dataList != null && dataList.size() > 0) {
                    Log.e(getClass().getName(), "Complain number found");
                    updateComplaintNumber(dataList);
                    updated = true;
                }
                break;
            case EMERGENCY_HELP:
                dataList = decodeStringList(content);
                if (dataList != null && dataList.size() > 0) {
                    Log.e(getClass().getName(), "emergency number found");
                    updateEmergencyNumber(dataList, "emergency");
                    updated = true;
                }
                break;
            case EXTORTION_HELP:
                dataList = decodeStringList(content);
                if (dataList != null && dataList.size() > 0) {
                    Log.e(getClass().getName(), "extortion number found");
                    updateEmergencyNumber(dataList, "extortion");
                    updated = true;
                }
                break;
            case POLICE_CLEARANCE_CERTIFICATE:
                String data = decodeStringEng(content);
                if (data != null && !data.equals("") && !data.equals("null")) {
                    PoliceClearanceCertificateTable policeClearanceCertificateTable =
                            PoliceClearanceCertificateTable
                                    .load(PoliceClearanceCertificateTable.class, 1);
                    //Log.e(getClass().getName(), data);
                    policeClearanceCertificateTable.setUrlLink(data);
                    // Log.e(getClass().getName(), policeClearanceCertificateTable.getUrlLink());
                    policeClearanceCertificateTable.save();
                }
        }
    }

    private void updateWeRemember(List<String> dataList, String history, int id) {
        WeRememberTable weRememberTable = WeRememberTable.load(WeRememberTable.class, id);
        String name = weRememberTable.getName();
        String designation = weRememberTable.getDesignation();
        String from = weRememberTable.getFromDate();
        String to = weRememberTable.getToDate();
        String deatils = weRememberTable.getDetails();
        if (name != null && !name.equals(dataList.get(0))) {
            weRememberTable.setName(dataList.get(0));
            Log.e(getClass().getName(), "We remember updated");
        }
        if (designation != null && !designation.equals(dataList.get(1))) {
            weRememberTable.setDesignation(dataList.get(1));
            Log.e(getClass().getName(), "We remember updated");
        }
        if (from != null && !from.equals(dataList.get(2))) {
            weRememberTable.setFromDate(dataList.get(2));
            Log.e(getClass().getName(), "We remember updated");
        }
        if (to != null && !to.equals(dataList.get(3))) {
            weRememberTable.setToDate(dataList.get(3));
            Log.e(getClass().getName(), "We remember updated");
        }
        if (deatils != null && !deatils.equals(history)) {
            weRememberTable.setDetails(history);
            Log.e(getClass().getName(), "We remember updated");
        }
        weRememberTable.save();

    }

    private void updateNews(List<String> dataList) {
        List<DMPPressTable> dmpPressTableList = DMPPressTable.getDmpPressList();
        String headline = null;
        String date = null;
        String details = null;
        int j = 0;
        if (dmpPressTableList != null && dmpPressTableList.size() > 0) {
            try {
                for (int i = 0; i < dataList.size(); i += 3) {
                    Log.e(getClass().getName(), "News Retrieved " + i);
                    headline = dataList.get(i);
                    date = dataList.get(i + 1);
                    details = dataList.get(i + 2);
                    if (i >= dmpPressTableList.size() * 3) {
                        DMPPressTable dmpPressTable = new DMPPressTable(headline, details, date);
                        Log.e(getClass().getName(), "News Inserted");
                    } else {
                        j++;
                        DMPPressTable dmpPressTable = DMPPressTable.load(DMPPressTable.class, j);
                        if (dmpPressTable != null) {

                            if (dmpPressTable.getHeadline() != null &&
                                    !dmpPressTable
                                            .getHeadline()
                                            .trim()
                                            .equals(headline)) {
                                dmpPressTable.setHeadline(headline);
                                Log.e(getClass().getName(), "News updated");
                            }
                            if (dmpPressTable.getDetails() != null && details != null &&
                                    !dmpPressTable
                                            .getDetails()
                                            .trim()
                                            .equals(details)) {
                                dmpPressTable.setDetails(details);
                                Log.e(getClass().getName(), "News updated");
                            }

                            if (dmpPressTable.getDate() != null && date != null &&
                                    !dmpPressTable
                                            .getDate()
                                            .trim()
                                            .equals(date)) {
                                dmpPressTable.setDate(date);
                                Log.e(getClass().getName(), "News updated");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void updateNewInitiativesInfo(String data, int i) {
        NewInitiativesTable newInitiativesTable = NewInitiativesTable
                .load(NewInitiativesTable.class, i);
        if (newInitiativesTable != null && !newInitiativesTable
                .getInitiatives()
                .trim()
                .equals(data)) {
            newInitiativesTable.setInitiatives(data);
            Log.e(getClass().getName(), "New Initiatives has been updated");
        }
        newInitiativesTable.save();
    }

    private void updatePoliceStationList() {
        if (policeStationInfoList != null) {
            for (int i = 0; i < policeStationInfoList.size(); i++) {
                PoliceThanaTable policeThanaTable = PoliceThanaTable.load(PoliceThanaTable.class,
                        i + 1);
                Boolean changed = false;
                DMPStationInfo dmpStationInfo = policeStationInfoList.get(i);
                if (policeThanaTable != null && dmpStationInfo != null) {
                    if (dmpStationInfo.getStattionName() != null &&
                            !policeThanaTable.getThanaName().trim()
                                    .equals(dmpStationInfo.getStattionName())) {
                        policeThanaTable.setThanaName(dmpStationInfo.getStattionName());
                        changed = true;
                    }
                    if (dmpStationInfo.getAddress() != null &&
                            !policeThanaTable.getAddress().trim()
                                    .equals(dmpStationInfo.getAddress())) {
                        policeThanaTable.setAddress(dmpStationInfo.getAddress());
                        changed = true;
                    }
                    if (dmpStationInfo.getDutyOfficers() != null &&
                            !policeThanaTable.getDutyOfficer().trim()
                                    .equals(dmpStationInfo.getDutyOfficers())) {
                        policeThanaTable.setDutyOfficer(dmpStationInfo.getDutyOfficers());
                        changed = true;
                    }
                    if (dmpStationInfo.getTandT_1() != null &&
                            !policeThanaTable.getT_and_t().trim()
                                    .equals(dmpStationInfo.getTandT_1())) {
                        policeThanaTable.setT_and_t(dmpStationInfo.getTandT_1());
                        changed = true;
                    }
                    if (dmpStationInfo.getMobile_1() != null &&
                            !policeThanaTable.getCell().trim()
                                    .equals(dmpStationInfo.getMobile_1())) {
                        policeThanaTable.setCell(dmpStationInfo.getMobile_1());
                        changed = true;
                    }
                    if (dmpStationInfo.getDMP_1() != null &&
                            !policeThanaTable.getDmp_1().trim()
                                    .equals(dmpStationInfo.getDMP_1())) {
                        policeThanaTable.setDmp_1(dmpStationInfo.getDMP_1());
                        changed = true;
                    }
                    if (dmpStationInfo.getInvestigationCell() != null &&
                            !policeThanaTable.getInspectorInvestigationCell().trim()
                                    .equals(dmpStationInfo.getInvestigationCell())) {
                        policeThanaTable.setInspectorInvestigationCell
                                (dmpStationInfo.getInvestigationCell());
                        changed = true;
                    }
                    if (dmpStationInfo.getDMP_2() != null &&
                            !policeThanaTable.getDmp_2().trim()
                                    .equals(dmpStationInfo.getDMP_2())) {
                        policeThanaTable.setDmp_2(dmpStationInfo.getDMP_2());
                        changed = true;
                    }
                    if (dmpStationInfo.getMobile_2() != null &&
                            !policeThanaTable.getMobile().trim()
                                    .equals(dmpStationInfo.getMobile_2())) {
                        policeThanaTable.setMobile(dmpStationInfo.getMobile_2());
                        changed = true;
                    }
                    if (dmpStationInfo.getFax() != null &&
                            !policeThanaTable.getFax().trim()
                                    .equals(dmpStationInfo.getFax())) {
                        policeThanaTable.setFax(dmpStationInfo.getFax());
                        changed = true;
                    }
                    if (dmpStationInfo.getEmail() != null &&
                            !policeThanaTable.getMail().trim()
                                    .equals(dmpStationInfo.getEmail())) {
                        policeThanaTable.setMail(dmpStationInfo.getEmail());
                        changed = true;
                    }
                    if (policeThanaTable.getLatitude() != dmpStationInfo.getLatitude()) {
                        policeThanaTable.setLatitude(dmpStationInfo.getLatitude());
                        changed = true;
                    }
                    if (policeThanaTable.getLongitude() != dmpStationInfo.getLongtitude()) {
                        policeThanaTable.setLongitude(dmpStationInfo.getLongtitude());
                        changed = true;
                    }
                }
                if (changed) {
                    Log.e(getClass().getName(), "Changed Police Station");
                    policeThanaTable.save();

                } else {
                    Log.e(getClass().getName(), "No Update");
                }
            }
        }
    }

    private void findOutPoliceStationInfo(String content) {
        DMPStationInfo dmpStationInfo = new DMPStationInfo();
        int count = 0;
        StringBuffer info = null;
        Boolean skip = false;
        char character;
        //Log.e(getClass().getName(), content);

        for (int i = 0; i < content.length(); i++) {
            character = content.charAt(i);
            if (character == '<' || character == '&') {
                skip = true;
                continue;
            } else if (character == '>') {
                skip = false;
                if (info != null && !info.toString().equals("")) {
                    addPoliceStationInfo(info, count, dmpStationInfo);
                    //Log.e(getClass().getName(), "Gone");
                }
                count++;
                info = new StringBuffer();
                info.append("");
                continue;
            } else if (character == ';') {
                skip = false;
                continue;
            }
            if (!skip) {
                info.append(Character.toString(character));
            }

        }
        if (dmpStationInfo != null) {
            policeStationInfoList.add(dmpStationInfo);
        }
    }

    private void addPoliceStationInfo(StringBuffer stringBuffer,
                                      int count, DMPStationInfo dmpStationInfo) {
        String info = stringBuffer.toString().trim();
        //Log.e(getClass().getName(), info);
        switch (count) {
            case 1:
                dmpStationInfo.setStattionName(info);
                break;
            case 2:
                dmpStationInfo.setAddress(info);
                break;
            case 3:
                dmpStationInfo.setDutyOfficers(info);
                break;
            case 4:
                dmpStationInfo.setTandT_1(info);
                break;
            case 5:
                dmpStationInfo.setMobile_1(info);
                break;
            case 6:
                dmpStationInfo.setDMP_1(info);
                break;
            case 7:
                dmpStationInfo.setInvestigationCell(info);
                break;
            case 8:
                dmpStationInfo.setDMP_2(info);
                break;
            case 9:
                dmpStationInfo.setMobile_2(info);
                break;
            case 10:
                dmpStationInfo.setFax(info);
                break;
            case 11:
                dmpStationInfo.setEmail(info);
                break;
            case 12:
                dmpStationInfo.setLatitude(Double.parseDouble(info));
                break;
            case 13:
                dmpStationInfo.setLongtitude(Double.parseDouble(info));
                break;
            default:
                break;
        }

    }


    private void updateEmergencyNumber(List<String> dataList, String type) {
        List<ImportantNumbersTable> importantNumbersTableList = ImportantNumbersTable
                .importantNumbersTableDataList(type);
        if (importantNumbersTableList != null && importantNumbersTableList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                String phNumber = dataList.get(i);
                Log.e(getClass().getName(), "Number retreived");
                if (i >= importantNumbersTableList.size()) {
                    ImportantNumbersTable importantNumbersTable = new
                            ImportantNumbersTable(type, phNumber);
                    Log.e(getClass().getName(), "Number inserted");
                } else {
                    ImportantNumbersTable importantNumbersTable = ImportantNumbersTable
                            .load(ImportantNumbersTable.class, importantNumbersTableList
                                    .get(i).getId());
                    if (importantNumbersTable != null && !importantNumbersTable
                            .getNumber()
                            .trim()
                            .equals(phNumber)) {
                        importantNumbersTable.setNumber(phNumber);
                        Log.e(getClass().getName(), "Updated Number");
                    }
                    importantNumbersTable.save();
                }
            }
        }

    }

    private void updateComplaintNumber(List<String> dataList) {
        List<ComplaintTable> complaintTableList = ComplaintTable.getComplaintPhNumber();
        if (complaintTableList != null && complaintTableList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                String number = dataList.get(i);
                Log.e(getClass().getName(), "Complain number found");
                if (i >= dataList.size()) {
                    ComplaintTable complaintTable = new ComplaintTable(number);
                    Log.e(getClass().getName(), "Complain number inserted");
                } else {
                    ComplaintTable complaintTable = ComplaintTable.load(ComplaintTable.class,
                            i + 1);
                    if (complaintTable != null && complaintTable.getPhNumber() != null
                            && !complaintTable
                            .getPhNumber()
                            .trim()
                            .equals(number)) {
                        Log.e(getClass().getName(), "Complain number updated");
                        complaintTable.setPhNumber(number);
                    }
                    complaintTable.save();
                }
            }
        }

    }


    private void updateDMPCommissionerMenu(String res, int columnNO) {
        DmpCommissionerTable dmpCommissionerTable =
                DmpCommissionerTable.load(DmpCommissionerTable.class, 1);
        switch (columnNO) {
            case 1:
                if (!dmpCommissionerTable.getMessage().trim().equals(res)) {
                    dmpCommissionerTable.setMessage(res);
                    Log.e(getClass().getName(), "updated");
                }
                break;
            case 2:
                if (!dmpCommissionerTable.getBiography().trim().equals(res)) {
                    dmpCommissionerTable.setBiography(res);
                    Log.e(getClass().getName(), "updated");
                }

                break;
            case 3:
                if (!dmpCommissionerTable.getImageLink().trim().equals(res)) {
                    dmpCommissionerTable.setImageLink(res);
                    Log.e(getClass().getName(), "updated");
                }
                break;
        }
        dmpCommissionerTable.save();
    }

    private String decodeStringEng(String content) {
        String res = null;
        if (content != null) {
            res = "";
            boolean nextStep = false;
            for (int i = 0; i < content.length(); i++) {
                if (content.charAt(i) == '<') {
                    nextStep = true;
                    continue;
                } else if (content.charAt(i) == '>') {
                    nextStep = false;
                    continue;
                } else {
                    if (!nextStep) {
                        res = res + Character.toString(content.charAt(i));
                    }
                }
            }
        }
        return res;

    }


    private String decodeString(String content) {
        String res = null;
        if (content != null) {
            res = "";
            boolean nextStep = false;
            for (int i = 0; i < content.length(); i++) {
                if (content.charAt(i) == '<' || content.charAt(i) == '&') {
                    nextStep = true;
                    continue;
                } else if (content.charAt(i) == '>') {
                    nextStep = false;
                    continue;
                } else if (content.charAt(i) == ';') {
                    nextStep = false;
                    res = res + Character.toString(' ');
                    continue;
                } else if ((content.charAt(i) >= 'A' && content.charAt(i) <= 'Z') ||
                        (content.charAt(i) >= 'a' && content.charAt(i) <= 'z') ||
                        (content.charAt(i) >= '0' && content.charAt(i) <= '9') ||
                        content.charAt(i) == '-' || content.charAt(i) == '"' ||
                        content.charAt(i) == ':') {
                    continue;
                } else {
                    if (!nextStep) {
                        if (content.charAt(i) == '|') {
                            res = res + Character.toString('।');
                        } else {
                            res = res + Character.toString(content.charAt(i));
                        }
                    }
                }
            }
        }
        return res;
    }

}