package com.nationalappsbd.dhakametropolitanpolice.webservices;

import android.util.Log;

import com.nationalappsbd.dhakametropolitanpolice.interfaces.Initializer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


/**
 * Created by Shuvojit Saha Shuvo on 1/29/2015.
 */
public class WebClient implements Initializer {
    private HttpClient httpClient;
    private HttpPost httpPost;
    private HttpResponse httpResponse;
    private HttpEntity httpEntity;
    private String webUrl;
    private String jsonData;

    public WebClient(String webUrl) {
        this.webUrl = webUrl;
        Log.e(getClass().getName(), webUrl);
        initialize();
    }

    public String getJSONStringData() {
        setJsonData();
        if (jsonData == null) {
            Log.e(getClass().getName(), " null is json Data");
        } else {
            Log.e(getClass().getName(), "json Data not null");
        }
        return jsonData;
    }

    @Override
    public void initialize() {
        httpClient = null;
        httpEntity = null;
        httpPost = null;
        httpResponse = null;
        jsonData = null;
    }

    private void setJsonData() {
        try {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost(webUrl);
            httpResponse = httpClient.execute(httpPost);
            int responseStatus = httpResponse.getStatusLine().getStatusCode();
            if (responseStatus == 200) {
                Log.e(getClass().getName(), String.valueOf(responseStatus));
                Log.e(getClass().getName(), "Data found");
                httpEntity = httpResponse.getEntity();
                jsonData = EntityUtils.toString(httpEntity);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
