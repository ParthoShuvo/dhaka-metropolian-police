package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by shuvojit on 5/28/15.
 */

@Table(name = "clearance_certificate")
public class PoliceClearanceCertificateTable extends Model {

    @Column(name = "url_link")
    private String urlLink;

    public PoliceClearanceCertificateTable() {
        super();
    }

    public static PoliceClearanceCertificateTable getPoliceClearanceCertificateTableData() {
        PoliceClearanceCertificateTable policeClearanceCertificateTableData = null;
        policeClearanceCertificateTableData = new Select().
                from(PoliceClearanceCertificateTable.class).
                executeSingle();
        return policeClearanceCertificateTableData;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }
}
