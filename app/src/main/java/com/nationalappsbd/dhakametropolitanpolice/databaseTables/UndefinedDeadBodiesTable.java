package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/9/15.
 */

@Table(name = "undefined_dead_bodies")
public class UndefinedDeadBodiesTable extends Model {

    @Column(name = "name")
    private String name;

    @Column(name = "image_link")
    private String imageLink;

    public UndefinedDeadBodiesTable(String name, String imageLink) {
        super();
        this.name = name;
        this.imageLink = imageLink;
        this.save();
    }

    public UndefinedDeadBodiesTable() {
        super();
    }

    public static List<UndefinedDeadBodiesTable> getAllUndefinedDeadBodies() {
        List<UndefinedDeadBodiesTable> undefinedDeadBodies = null;
        undefinedDeadBodies = new Select().from(UndefinedDeadBodiesTable.class).execute();
        return undefinedDeadBodies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
