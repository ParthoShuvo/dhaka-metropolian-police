package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/8/15.
 */

@Table(name = "important_number_table")
public class ImportantNumbersTable extends Model implements Serializable {

    @Column(name = "type")
    private String type;
    @Column(name = "number")
    private String number;

    public ImportantNumbersTable(String type, String number) {
        super();
        this.type = type;
        this.number = number;
        this.save();
    }

    public ImportantNumbersTable() {
        super();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public static List<ImportantNumbersTable> importantNumbersTableDataList(String type) {
        List<ImportantNumbersTable> importantNumbersTableDataList = null;
        importantNumbersTableDataList = new Select().from(ImportantNumbersTable.class)
                .where("type = ?", new String[]{type}).execute();
        return importantNumbersTableDataList;
    }
}
