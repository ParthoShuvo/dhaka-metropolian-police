package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/15/15.
 */

@Table(name = "dmp_services")
public class DMPServicesTable extends Model implements Serializable {

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "details")
    private String details;

    public DMPServicesTable() {
        super();
    }

    public DMPServicesTable(String serviceName, String details) {
        super();
        this.serviceName = serviceName;
        this.details = details;
    }

    public static List<DMPServicesTable> getdmpServicesTableList() {
        List<DMPServicesTable> dmpServicesTableList = null;
        dmpServicesTableList = new Select().from(DMPServicesTable.class).execute();
        return dmpServicesTableList;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }


}
