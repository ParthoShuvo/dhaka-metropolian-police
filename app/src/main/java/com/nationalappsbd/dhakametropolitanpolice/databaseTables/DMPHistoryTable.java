package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/15/15.
 */


@Table(name = "dmp_history")
public class DMPHistoryTable extends Model implements Serializable {

    @Column(name = "history")
    private String history;

    public DMPHistoryTable() {
        super();
    }

    public DMPHistoryTable(String history) {
        super();
        this.history = history;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }


}
