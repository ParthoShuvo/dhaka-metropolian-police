package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */

@Table(name = "new_initiatives")
public class NewInitiativesTable extends Model implements Serializable {

    @Column(name = "initiatives")
    private String initiatives;

    public NewInitiativesTable() {
        super();
    }

    public NewInitiativesTable(final String initiatives) {
        super();
        this.initiatives = initiatives;
        this.save();
    }

    public static List<NewInitiativesTable> getNewInitiativesTableDataList() {
        List<NewInitiativesTable> newInitiativesTableDataList = null;
        newInitiativesTableDataList = new Select().from(NewInitiativesTable.class).execute();
        return newInitiativesTableDataList;
    }

    public String getInitiatives() {
        return initiatives;
    }

    public void setInitiatives(String initiatives) {
        this.initiatives = initiatives;
    }
}
