package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */

@Table(name = "dmp_parters")
public class DMP_PartersTable extends Model implements Serializable{

    @Column(name = "parter_name")
    private String parterName;

    @Column(name = "image_link")
    private String imageLink;

    public DMP_PartersTable() {
        super();
    }

    public DMP_PartersTable(final String parterName, String imageLink) {
        super();
        this.parterName = parterName;
        this.imageLink = imageLink;
        this.save();
    }

    public static List<DMP_PartersTable> getPartersDataList() {
        List<DMP_PartersTable> dmpPartnerDataList = null;
        dmpPartnerDataList = new Select().from(DMP_PartersTable.class).execute();
        return dmpPartnerDataList;
    }

    public String getParterName() {

        return parterName;
    }

    public void setParterName(String parterName) {
        this.parterName = parterName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
