package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/8/15.
 */
@Table(name = "we_remember")
public class WeRememberTable extends Model implements Serializable{

    @Column(name = "name")
    private String name;

    @Column(name = "Designation")
    private String designation;

    @Column(name = "from_date")
    private String fromDate;

    @Column(name = "to_date")
    private String toDate;

    @Column(name = "details")
    private String details;


    public WeRememberTable() {
        super();
    }

    public WeRememberTable(String name, String designation,
                           String fromDate, String toDate, String details) {
        super();
        this.name = name;
        this.designation = designation;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.details = details;
        this.save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static List<WeRememberTable> getWeRememberTableDataList() {
        List<WeRememberTable> weRememberTableDataList = null;
        weRememberTableDataList = new Select().from(WeRememberTable.class).execute();
        return weRememberTableDataList;
    }

}
