package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/9/15.
 */

@Table(name = "dmp_image_gallery")
public class ImageGalleryTable extends Model {

    @Column(name = "image_link")
    private String imageLink;

    @Column(name = "image_discription")
    private String imageDescription;

    public ImageGalleryTable() {
        super();
    }

    public ImageGalleryTable(String imageLink, String imageDescription) {
        super();
        this.imageLink = imageLink;
        this.imageDescription = imageDescription;
        this.save();
    }

    public static List<ImageGalleryTable> getImageGalleryTableDataList() {
        List<ImageGalleryTable> imageGalleryTableList = null;
        imageGalleryTableList = new Select().from(ImageGalleryTable.class).execute();
        return imageGalleryTableList;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }
}
