package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/9/15.
 */

@Table(name = "commissioner_list")
public class CommissionerListTable extends Model {

    @Column(name = "name")
    private String name;

    @Column(name = "image_link")
    private String imageLink;


    @Column(name = "working_period")
    private String workingPeriod;

    public CommissionerListTable(String name, String imageLink, String workingPeriod) {
        super();
        this.name = name;
        this.imageLink = imageLink;
        this.workingPeriod = workingPeriod;
        this.save();
    }

    public CommissionerListTable() {
        super();
    }

    public static List<CommissionerListTable> getCommissionerList() {
        List<CommissionerListTable> commissionerList = null;
        commissionerList = new Select().from(CommissionerListTable.class).execute();
        return commissionerList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getWorkingPeriod() {
        return workingPeriod;
    }

    public void setWorkingPeriod(String workingPeriod) {

        this.workingPeriod = workingPeriod;
    }
}
