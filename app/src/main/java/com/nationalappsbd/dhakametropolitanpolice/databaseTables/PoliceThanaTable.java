package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuvojit on 5/8/15.
 */

@Table(name = "police_thana")
public class PoliceThanaTable extends Model implements Serializable {

    @Column(name = "thana_name")
    private String thanaName;

    @Column(name = "address")
    private String address;

    @Column(name = "duty_officer")
    private String dutyOfficer;

    @Column(name = "t_and_t")
    private String t_and_t;

    @Column(name = "cell")
    private String cell;

    @Column(name = "dmp_1")
    private String dmp_1;

    @Column(name = "inspector_investigation_cell")
    private String inspectorInvestigationCell;

    @Column(name = "officers_in_charge")
    private String officersInCharge;

    @Column(name = "dmp_2")
    private String dmp_2;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "fax")
    private String fax;

    @Column(name = "mail")
    private String mail;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    public PoliceThanaTable() {
        super();
    }

    public PoliceThanaTable(String thanaName, String address, String dutyOfficer,
                            String t_and_t, String cell, String dmp_1,
                            String inspectorInvestigationCell, String officersInCharge,
                            String dmp_2, String mobile, String fax, String mail,
                            double latitude, double longitude) {
        super();
        this.thanaName = thanaName;
        this.address = address;
        this.dutyOfficer = dutyOfficer;
        this.t_and_t = t_and_t;
        this.cell = cell;
        this.dmp_1 = dmp_1;
        this.inspectorInvestigationCell = inspectorInvestigationCell;
        this.officersInCharge = officersInCharge;
        this.dmp_2 = dmp_2;
        this.mobile = mobile;
        this.fax = fax;
        this.mail = mail;
        this.latitude = latitude;
        this.longitude = longitude;
        this.save();
    }

    public static List<PoliceThanaTable> getAllPoliceThanaList() {
        List<PoliceThanaTable> policeThanaTableList = null;
        policeThanaTableList = new Select().from(PoliceThanaTable.class).execute();
        return policeThanaTableList;
    }

    public static List<PoliceThanaTable> getAllPoliceThanaList(String thanaName) {
        List<PoliceThanaTable> policeThanaTableList = null;
        policeThanaTableList = new Select().from(PoliceThanaTable.class)
                .where("thana_name = ?", new String[]{thanaName}).execute();
        return policeThanaTableList;
    }

    public static List<String> getAllPoliceThanaNameList() {
        List<String> policeStationNameList = new ArrayList<String>();
        List<PoliceThanaTable> policeThanaTableList = new Select().
                from(PoliceThanaTable.class)
                .execute();
        if (policeThanaTableList != null && policeThanaTableList.size() > 0) {
            for (int i = 0; i < policeThanaTableList.size(); i++) {
                policeStationNameList.add(policeThanaTableList.get(i).getThanaName());
            }
        }
        return policeStationNameList;
    }

    public static List<String> getAllPoliceThanaNameList(String queryString) {
        List<String> policeStationNameList = null;
        if (queryString != null && !queryString.equals("")) {
            List<PoliceThanaTable> policeThanaTableList = new Select()
                    .from(PoliceThanaTable.class)
                    .where("thana_name LIKE '" + queryString + "%'")
                    .execute();
            if (policeThanaTableList != null && policeThanaTableList.size() > 0) {
                policeStationNameList = new ArrayList<String>();
                for (int i = 0; i < policeThanaTableList.size(); i++) {
                    policeStationNameList.add(policeThanaTableList.get(i).getThanaName());

                }
            }
        }
        return policeStationNameList;
    }


    public String getThanaName() {
        return thanaName;
    }

    public void setThanaName(String thanaName) {
        this.thanaName = thanaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDutyOfficer() {
        return dutyOfficer;
    }

    public void setDutyOfficer(String dutyOfficer) {
        this.dutyOfficer = dutyOfficer;
    }

    public String getT_and_t() {
        return t_and_t;
    }

    public void setT_and_t(String t_and_t) {
        this.t_and_t = t_and_t;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getDmp_1() {
        return dmp_1;
    }

    public void setDmp_1(String dmp_1) {
        this.dmp_1 = dmp_1;
    }

    public String getInspectorInvestigationCell() {
        return inspectorInvestigationCell;
    }

    public void setInspectorInvestigationCell(String inspectorInvestigationCell) {
        this.inspectorInvestigationCell = inspectorInvestigationCell;
    }

    public String getOfficersInCharge() {
        return officersInCharge;
    }

    public void setOfficersInCharge(String officersInCharge) {
        this.officersInCharge = officersInCharge;
    }

    public String getDmp_2() {
        return dmp_2;
    }

    public void setDmp_2(String dmp_2) {
        this.dmp_2 = dmp_2;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


}
