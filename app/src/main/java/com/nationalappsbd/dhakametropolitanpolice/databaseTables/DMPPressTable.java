package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/10/15.
 */

@Table(name = "dmp_press")
public class DMPPressTable extends Model implements Serializable {

    @Column(name = "headline")
    private String headline;

    @Column(name = "details")
    private String details;

    @Column(name = "date")
    private String date;


    public DMPPressTable() {
        super();
    }

    public DMPPressTable(String headline, String details, String date) {
        super();
        this.headline = headline;
        this.details = details;
        this.date = date;
        this.save();
    }

    public static List<DMPPressTable> getDmpPressList() {
        List<DMPPressTable> dmpPressList = null;
        dmpPressList = new Select().from(DMPPressTable.class).execute();
        return dmpPressList;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
