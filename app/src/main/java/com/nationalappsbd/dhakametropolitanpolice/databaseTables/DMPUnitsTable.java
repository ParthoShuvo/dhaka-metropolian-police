package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/7/15.
 */

@Table(name = "dmp_units")
public class DMPUnitsTable extends Model implements Serializable{

    @Column(name = "unit")
    private String unit;

    @Column(name = "details")
    private String details;

    public DMPUnitsTable(String unit, String details) {
        super();
        this.unit = unit;
        this.details = details;
        this.save();
    }

    public DMPUnitsTable() {
        super();
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDetails() {
        return details;
    }

    public static DMPUnitsTable getDMUnitsTableData(int Id) {
        DMPUnitsTable dmpUnitsTable = null;
        dmpUnitsTable = new Select().from(DMPUnitsTable.class).
                where("Id = " + Id).executeSingle();
        return dmpUnitsTable;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
