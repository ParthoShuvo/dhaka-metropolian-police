package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/9/15.
 */

@Table(name = "Complaint")
public class ComplaintTable extends Model {

    @Column(name = "ph_number")
    private String phNumber;

    public ComplaintTable() {
        super();
    }

    public ComplaintTable(String phNumber) {
        super();
        this.phNumber = phNumber;
        this.save();
    }

    public static List<ComplaintTable> getComplaintPhNumber() {
        List<ComplaintTable> complaintPhNumbers = null;
        complaintPhNumbers = new Select().from(ComplaintTable.class).execute();
        return complaintPhNumbers;
    }

    public String getPhNumber() {
        return phNumber;
    }

    public void setPhNumber(String phNumber) {
        this.phNumber = phNumber;
    }
}
