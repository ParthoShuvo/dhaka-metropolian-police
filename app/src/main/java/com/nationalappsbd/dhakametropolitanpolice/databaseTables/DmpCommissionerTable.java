package com.nationalappsbd.dhakametropolitanpolice.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by shuvojit on 5/9/15.
 */

@Table(name = "dmp_commisioner")
public class DmpCommissionerTable extends Model {

    @Column(name = "message")
    private String message;

    @Column(name = "biography")
    private String biography;

    @Column(name = "image_link")
    private String imageLink;

    public DmpCommissionerTable() {
        super();
    }

    public DmpCommissionerTable(String message, String biography, String imageLink) {
        super();
        this.message = message;
        this.biography = biography;
        this.imageLink = imageLink;
        this.save();
    }

    public static DmpCommissionerTable getDmpCommissioner() {
        DmpCommissionerTable dmpCommissionerTable = new Select().
                from(DmpCommissionerTable.class).executeSingle();
        return dmpCommissionerTable;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
