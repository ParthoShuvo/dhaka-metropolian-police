package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;

/**
 * Created by shuvojit on 5/16/15.
 */
public class GalleryImageContents implements JsonFields {

    @SerializedName(GALLERY_ID)
    private String galleryID;

    @SerializedName(app_id)
    private String appID;

    @SerializedName(imagepath)
    private String imagePath;

    @SerializedName(DESCRIPTION)
    private String description;

    @SerializedName(GALLERY_TYPE)
    private String galleryType;

    @SerializedName(ACTIVE)
    private String active;

    public GalleryImageContents() {
    }

    public GalleryImageContents(String galleryID, String appID,
                                String imagePath, String description,
                                String galleryType, String active) {
        this.galleryID = galleryID;
        this.appID = appID;
        this.imagePath = imagePath;
        this.description = description;
        this.galleryType = galleryType;
        this.active = active;
    }

    public String getGalleryID() {
        return galleryID;
    }

    public void setGalleryID(String galleryID) {
        this.galleryID = galleryID;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGalleryType() {
        return galleryType;
    }

    public void setGalleryType(String galleryType) {
        this.galleryType = galleryType;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
