package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;

import java.util.ArrayList;

/**
 * Created by shuvojit on 5/15/15.
 */
public class Menu implements JsonFields {

    @SerializedName(menu)
    private ArrayList<MenuContents> menuContentsArrayList;

    public Menu() {
    }

    public Menu(ArrayList<MenuContents> menuContentsArrayList) {
        this.menuContentsArrayList = menuContentsArrayList;
    }

    public ArrayList<MenuContents> getMenuContentsArrayList() {
        return menuContentsArrayList;
    }

    public void setMenuContentsArrayList(ArrayList<MenuContents> menuContentsArrayList) {
        this.menuContentsArrayList = menuContentsArrayList;
    }
}
