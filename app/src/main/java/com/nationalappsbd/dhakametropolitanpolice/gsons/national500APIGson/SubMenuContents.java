package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;

/**
 * Created by shuvojit on 5/14/15.
 */
public class SubMenuContents implements JsonFields {

    @SerializedName(submenu_id)
    private String subMenuID;

    @SerializedName(submenu_name)
    private String subMenuName;

    @SerializedName(menuId)
    private String menuID;

    @SerializedName(CONTENT)
    private String content;

    public SubMenuContents(String subMenuID, String subMenuName,
                           String menuID, String content) {
        this.subMenuID = subMenuID;
        this.subMenuName = subMenuName;
        this.menuID = menuID;
        this.content = content;
    }

    public SubMenuContents() {
    }

    public String getSubMenuID() {
        return subMenuID;
    }

    public void setSubMenuID(String subMenuID) {
        this.subMenuID = subMenuID;
    }

    public String getSubMenuName() {
        return subMenuName;
    }

    public void setSubMenuName(String subMenuName) {
        this.subMenuName = subMenuName;
    }

    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
