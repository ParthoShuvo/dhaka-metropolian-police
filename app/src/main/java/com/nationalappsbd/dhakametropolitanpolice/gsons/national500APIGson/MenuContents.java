package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;

/**
 * Created by shuvojit on 5/15/15.
 */
public class MenuContents implements JsonFields {

    @SerializedName(menuId)
    private String menuID;

    @SerializedName(menu_name)
    private String menuName;

    @SerializedName(app_code)
    private String appCode;

    @SerializedName(app_id)
    private String appID;

    @SerializedName(menu_type)
    private String menuType;

    @SerializedName(CONTENT)
    private String content;

    @SerializedName(ACTIVE)
    private String active;

    @SerializedName(submenuavailable)
    private String hasSubMenu;

    public MenuContents() {
    }

    public MenuContents(String menuID, String menuName,
                        String appCode, String appID, String menuType,
                        String content, String active, String hasSubMenu) {
        this.menuID = menuID;
        this.menuName = menuName;
        this.appCode = appCode;
        this.appID = appID;
        this.menuType = menuType;
        this.content = content;
        this.active = active;
        this.hasSubMenu = hasSubMenu;
    }

    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getHasSubMenu() {
        return hasSubMenu;
    }

    public void setHasSubMenu(String hasSubMenu) {
        this.hasSubMenu = hasSubMenu;
    }
}
