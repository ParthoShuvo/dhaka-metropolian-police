package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonLinks;

import java.util.ArrayList;

/**
 * Created by shuvojit on 5/14/15.
 */
public class SubMenu implements JsonLinks {

    @SerializedName("Submenu")
    private ArrayList<SubMenuContents> subMenuContentsArrayList;

    public SubMenu(ArrayList<SubMenuContents> subMenuContentsArrayList) {
        this.subMenuContentsArrayList = subMenuContentsArrayList;
    }

    public SubMenu() {
    }

    public ArrayList<SubMenuContents> getSubMenuContentsArrayList() {
        return subMenuContentsArrayList;
    }

    public void setSubMenuContentsArrayList(ArrayList<SubMenuContents>
                                                    subMenuContentsArrayList) {
        this.subMenuContentsArrayList = subMenuContentsArrayList;
    }
}
