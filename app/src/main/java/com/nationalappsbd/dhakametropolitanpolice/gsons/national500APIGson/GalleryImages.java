package com.nationalappsbd.dhakametropolitanpolice.gsons.national500APIGson;

import com.google.gson.annotations.SerializedName;
import com.nationalappsbd.dhakametropolitanpolice.interfaces.JsonFields;

import java.util.ArrayList;

/**
 * Created by shuvojit on 5/16/15.
 */
public class GalleryImages implements JsonFields {

    @SerializedName(GALLERY_IMAGES)
    private ArrayList<GalleryImageContents> imageContentsArrayList;

    public GalleryImages() {
    }

    public GalleryImages(ArrayList<GalleryImageContents> imageContentsArrayList) {
        this.imageContentsArrayList = imageContentsArrayList;
    }

    public ArrayList<GalleryImageContents> getImageContentsArrayList() {
        return imageContentsArrayList;
    }

    public void setImageContentsArrayList(ArrayList<GalleryImageContents> imageContentsArrayList) {
        this.imageContentsArrayList = imageContentsArrayList;
    }
}
