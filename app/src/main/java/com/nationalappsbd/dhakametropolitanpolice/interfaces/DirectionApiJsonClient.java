package com.nationalappsbd.dhakametropolitanpolice.interfaces;

/**
 * Created by Shuvojit Saha Shuvo on 1/30/2015.
 */
public interface DirectionApiJsonClient {
    public static final String DIRECTION_API_WEB_ADDRESS_PART_1 = "http://maps.googleapis.com/" +
            "maps/api/directions/json?origin=";
    public static final String DIRECTION_API_WEB_ADDRESS_PART_2 = "&destination=";
    public static final String DIRECTION_API_WEB_ADDRESS_PART_3 = "&language=bn&sensor=false&" +
            "units=metric&mode=driving";

}
