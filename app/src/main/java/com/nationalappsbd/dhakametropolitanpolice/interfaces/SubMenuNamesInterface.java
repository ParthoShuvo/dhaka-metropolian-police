package com.nationalappsbd.dhakametropolitanpolice.interfaces;

/**
 * Created by shuvojit on 5/14/15.
 */
public interface SubMenuNamesInterface {

    public final static String DMP_SERVICES_1 = "ডিএমপির সেবাসমুহ 1";

    public final static String DMP_SERVICES_2 = "ডিএমপির সেবাসমুহ 2";

    public final static String DMP_SERVICES_3 = "ডিএমপির সেবাসমুহ 3";

    public final static String MESSAGE_OF_COMMISSIONER = "কমিশনারের বার্তা";

    public final static String BIOGRAPHY_OF_COMMISSIONER = "কমিশনার এর জীবনী";

    public final static String IMAGE_LINK_OF_COMMISSIONER = "IMAGE LINK OF COMMISSIONER";

    public final static String NEW_INITIATIVE_1 = "নতুন উদ্যোগ 1";

    public final static String NEW_INITIATIVE_2 = "নতুন উদ্যোগ 2";

    public final static String NEW_INITIATIVE_3 = "নতুন উদ্যোগ 3";

    public final static String NEW_INITIATIVE_4 = "নতুন উদ্যোগ 4";

    public final static String NEW_INITIATIVE_5 = "নতুন উদ্যোগ 5";

    public final static String NEW_INITIATIVE_6 = "নতুন উদ্যোগ 6";

    public final static String NEW_INITIATIVE_7 = "নতুন উদ্যোগ 7";

    public final static String NEW_INITIATIVE_8 = "নতুন উদ্যোগ 8";

    public final static String NEW_INITIATIVE_9 = "নতুন উদ্যোগ 9";

    public final static String NEW_INITIATIVE_10 = "নতুন উদ্যোগ 10";

    public final static String DMP_PARTNERS_1 = "ডিএমপি অংশীদার 1";

    public final static String DMP_PARTNERS_2 = "ডিএমপি অংশীদার 2";

    public final static String DMP_PARTNERS_3 = "ডিএমপি অংশীদার 3";

    public final static String DMP_PARTNERS_4 = "ডিএমপি অংশীদার 4";

    public final static String DMP_PARTNERS_5 = "ডিএমপি অংশীদার 5";

    public final static String DMP_PARTNERS_6 = "ডিএমপি অংশীদার 6";

    public final static String DMP_PARTNERS_7 = "ডিএমপি অংশীদার 7";

    public final static String DMP_PARTNERS_8 = "ডিএমপি অংশীদার 8";

    public final static String DMP_PARTNERS_9 = "ডিএমপি অংশীদার 9";

    public final static String DMP_PARTNERS_10 = "ডিএমপি অংশীদার 10";

    public final static String DMP_PARTNERS_11 = "ডিএমপি অংশীদার 11";

    public final static String DMP_PARTNERS_12 = "ডিএমপি অংশীদার 12";

    public final static String DMP_INTERNAL_ORGANIZATION_1 = "অপরাধ ও অপারেশন বিভাগ";

    public final static String DMP_INTERNAL_ORGANIZATION_2 = "ডিএমপি সদর দপ্তর ও পাবলিক অর্ডার ম্যানেজমেন্ট";

    public final static String DMP_INTERNAL_ORGANIZATION_3 = "সুরক্ষা ও প্রোটোকল বিভাগ";

    public final static String DMP_INTERNAL_ORGANIZATION_4 = "ট্রাফিক বিভাগ";

    public final static String DMP_INTERNAL_ORGANIZATION_5 = "গোয়েন্দা ও ফৌজদারী গোয়েন্দা বিভাগ";

    public final static String DMP_HISTORY = "ঢাকা মহানগর পুলিশের ইতিহাস";

    public final static String MOST_WANTED_CRIMINALS = "মোস্ট ওয়ান্টেড অপরাধী";

    public final static String UNDEFINED_DEAD_BODIES = "অসনাক্তকৃত লাশ";

    public final static String PREVIOUS_DMP_COMMISSIONER = "সাবেক কমিশনারগণ";

    public final static String POLICE_STATION_1 = "পুলিশ থানাসমুহ 1";
    public final static String POLICE_STATION_2 = "পুলিশ থানাসমুহ 2";
    public final static String POLICE_STATION_3 = "পুলিশ থানাসমুহ 3";
    public final static String POLICE_STATION_4 = "পুলিশ থানাসমুহ 4";
    public final static String POLICE_STATION_5 = "পুলিশ থানাসমুহ 5";
    public final static String POLICE_STATION_6 = "পুলিশ থানাসমুহ 6";
    public final static String POLICE_STATION_7 = "পুলিশ থানাসমুহ 7";
    public final static String POLICE_STATION_8 = "পুলিশ থানাসমুহ 8";
    public final static String POLICE_STATION_9 = "পুলিশ থানাসমুহ 9";
    public final static String POLICE_STATION_10 = "পুলিশ থানাসমুহ 10";
    public final static String POLICE_STATION_11 = "পুলিশ থানাসমুহ 11";
    public final static String POLICE_STATION_12 = "পুলিশ থানাসমুহ 12";
    public final static String POLICE_STATION_13 = "পুলিশ থানাসমুহ 13";
    public final static String POLICE_STATION_14 = "পুলিশ থানাসমুহ 14";
    public final static String POLICE_STATION_15 = "পুলিশ থানাসমুহ 15";
    public final static String POLICE_STATION_16 = "পুলিশ থানাসমুহ 16";
    public final static String POLICE_STATION_17 = "পুলিশ থানাসমুহ 17";
    public final static String POLICE_STATION_18 = "পুলিশ থানাসমুহ 18";
    public final static String POLICE_STATION_19 = "পুলিশ থানাসমুহ 19";
    public final static String POLICE_STATION_20 = "পুলিশ থানাসমুহ 20";
    public final static String POLICE_STATION_21 = "পুলিশ থানাসমুহ 21";
    public final static String POLICE_STATION_22 = "পুলিশ থানাসমুহ 22";
    public final static String POLICE_STATION_23 = "পুলিশ থানাসমুহ 23";
    public final static String POLICE_STATION_24 = "পুলিশ থানাসমুহ 24";
    public final static String POLICE_STATION_25 = "পুলিশ থানাসমুহ 25";
    public final static String POLICE_STATION_26 = "পুলিশ থানাসমুহ 26";
    public final static String POLICE_STATION_27 = "পুলিশ থানাসমুহ 27";
    public final static String POLICE_STATION_28 = "পুলিশ থানাসমুহ 28";
    public final static String POLICE_STATION_29 = "পুলিশ থানাসমুহ 29";
    public final static String POLICE_STATION_30 = "পুলিশ থানাসমুহ 30";
    public final static String POLICE_STATION_31 = "পুলিশ থানাসমুহ 31";
    public final static String POLICE_STATION_32 = "পুলিশ থানাসমুহ 32";
    public final static String POLICE_STATION_33 = "পুলিশ থানাসমুহ 33";
    public final static String POLICE_STATION_34 = "পুলিশ থানাসমুহ 34";
    public final static String POLICE_STATION_35 = "পুলিশ থানাসমুহ 35";
    public final static String POLICE_STATION_36 = "পুলিশ থানাসমুহ 36";
    public final static String POLICE_STATION_37 = "পুলিশ থানাসমুহ 37";
    public final static String POLICE_STATION_38 = "পুলিশ থানাসমুহ 38";
    public final static String POLICE_STATION_39 = "পুলিশ থানাসমুহ 39";
    public final static String POLICE_STATION_40 = "পুলিশ থানাসমুহ 40";
    public final static String POLICE_STATION_41 = "পুলিশ থানাসমুহ 41";
    public final static String POLICE_STATION_42 = "পুলিশ থানাসমুহ 42";
    public final static String POLICE_STATION_43 = "পুলিশ থানাসমুহ 43";
    public final static String POLICE_STATION_44 = "পুলিশ থানাসমুহ 44";
    public final static String POLICE_STATION_45 = "পুলিশ থানাসমুহ 45";
    public final static String POLICE_STATION_46 = "পুলিশ থানাসমুহ 46";
    public final static String POLICE_STATION_47 = "পুলিশ থানাসমুহ 47";
    public final static String POLICE_STATION_48 = "পুলিশ থানাসমুহ 48";
    public final static String POLICE_STATION_49 = "পুলিশ থানাসমুহ 49";

























}
