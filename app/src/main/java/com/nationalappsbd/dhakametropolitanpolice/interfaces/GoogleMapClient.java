package com.nationalappsbd.dhakametropolitanpolice.interfaces;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by Shuvojit Saha Shuvo on 1/26/2015.
 */
public interface GoogleMapClient {

    public static final double BANGLADESH_LATITUDE = 23.7000;
    public static final double BANGLADESH_LONGITUDE = 90.3667;

    public void setGoogleMapZoom(double latitude, double longtitude, float cameraZoom);

    public void setMarkerColor(Marker marker);

    public void addMarkerOnGoogleMap();

    public void setGoogleMapUiSettings();

}
